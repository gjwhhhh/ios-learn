//
//  ViewController.m
//  CardGameWithNoStoryBoard
//
//  Created by ByteDance on 2022/6/27.
//

#import "ViewController.h"
#import "Deck.h"
#import "PlayingCardDeck.h"
#import "CardMachingGame.h"

@interface ViewController ()
@property (nonatomic, strong) Deck *deck;
@property (nonatomic, strong) CardMachingGame *game;
@property (nonatomic, strong) NSMutableArray *cardButtons;
@property (nonatomic, weak) UILabel *scoreLabel;
@property (nonatomic, weak) UITextView *detailTextView;
@end

@implementation ViewController

- (NSMutableArray *) cardButtons
{
    if(!_cardButtons)
        _cardButtons = [[NSMutableArray alloc] init];
    return _cardButtons;
}

- (CardMachingGame *)game
{
    if(!_game) _game = [[CardMachingGame alloc] initWithCardCount:[self.cardButtons count] usingDeck:[self createDeck]];
    return _game;
}

- (Deck *)createDeck
{
    return [[PlayingCardDeck alloc] init];
}

- (IBAction)touchCardButton:(UIButton *)sender {
    
    int cardIndex = [self.cardButtons indexOfObject:sender];
    NSLog(@"%d",cardIndex);
    [self.game chooseCardAtIndex:cardIndex];
    [self updateUI];
}

- (void)updateUI
{
    for(UIButton *cardButton in self.cardButtons){
        int cardIndex = [self.cardButtons indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardIndex];
        [cardButton setTitle:[self titleForCard:card] forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self backgroundImageForCard:card] forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
        }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d",self.game.score];
    self.detailTextView.text = self.game.detailString;
}


//创建一个UIButton
- (UIButton *)createCardButtonSetX:(CGFloat)x setY:(CGFloat)y setWidth:(CGFloat)w setHeight:(CGFloat)h
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(x,y,w,h)];
    [button setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [button setTitle:@"" forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor whiteColor]];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(touchCardButton:)
     forControlEvents:UIControlEventTouchUpInside];
    return button;
}
//初始化标题
- (void)titleInit
{
    CGRect screen=[[UIScreen mainScreen]bounds];
    CGFloat labelWidth=150;
    CGFloat labelHeight=20;
    CGFloat labelTopView=70;

    UILabel* label=[[UILabel alloc]initWithFrame:CGRectMake((screen.size.width-labelWidth)/2, labelTopView, labelWidth, labelHeight)];
    label.text=@"卡牌游戏";
    label.textAlignment=NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];

    [self.view addSubview:label];
}

//默认卡片的大小
static const CGFloat CARD_WIDTH = 75;
static const CGFloat CARD_HEIGHT = 120;
static const int CARD_NUM = 12;
static const int CARD_ROW = 3;
static const int CARD_COL = 4;
static const CGFloat CARD_SPACE = 15;
CGRect screen;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor systemGreenColor];//将背景设置为白色
    //初始化标题
    [self titleInit];
    screen = [[UIScreen mainScreen]bounds];

    int viewTopHeight = 100;
    int viewLeftDis = (screen.size.width - (CARD_COL*(CARD_WIDTH+CARD_SPACE)) + CARD_SPACE)/2;
    for(int i=0; i<CARD_ROW; ++i){
        for(int j=0; j<CARD_COL; ++j)
        {
            UIButton *button  = [self createCardButtonSetX:j*CARD_WIDTH+j*CARD_SPACE+viewLeftDis setY:i*CARD_HEIGHT+(i+1)*CARD_SPACE+viewTopHeight
                                                  setWidth:CARD_WIDTH setHeight:CARD_HEIGHT];
            [self.view addSubview:button];
            [self.cardButtons addObject:button];
        }
    }
}


- (NSString *)titleForCard:(Card *)card
{
    return card.isChosen ? card.contents : @"";
}

- (UIImage *)backgroundImageForCard:(Card *)card
{
    return [UIImage imageNamed:card.isChosen ? nil : @"back"];
}
@end
