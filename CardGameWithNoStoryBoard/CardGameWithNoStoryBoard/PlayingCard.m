//
//  PlayingCard.m
//  ios-app-01
//
//  Created by ByteDance on 2022/6/26.
//

#import "PlayingCard.h"

@implementation PlayingCard

- (int)match:(NSArray *)otherCards  //纸牌的匹配逻辑
{
    int score = 0;
    if([otherCards count] == 1){
        id card = [otherCards firstObject];  //这里使用id类型，接收NSArray的返回值
        if([card isKindOfClass:[PlayingCard class]]){ //在这里判断是否是纸牌类型
            PlayingCard *otherCard = (PlayingCard *)card;
            if([self.suit isEqualToString:otherCard.suit]){  //如果花色匹配，那么得一分
                score = 1;
            }else if(self.rank == otherCard.rank){ //如果数字匹配得4分
                score = 4;
            }
        }
        
    }
    return score;
}
- (NSString *)contents
{

    NSArray *rankStrings = [PlayingCard rankStrings];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}

@synthesize suit = _suit;  //因为重写了suit的getter和setter方法，因此要加这个

+ (NSArray *)validSuits
{
    return @[@"♣️",@"♦️",@"♥️",@"♠️"];
}
+ (NSArray *)rankStrings
{
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",
             @"J",@"Q",@"K"];
}
+ (NSUInteger)maxRank
{
    return [[self rankStrings] count] - 1;
}
-(void)setSuit:(NSString *)suit
{
    if([[PlayingCard validSuits] containsObject:suit]){
        _suit = suit;
    }
}
-(NSString *)suit
{
    return _suit!=nil ? _suit : @"?";
}

-(void)setRank:(NSUInteger)rank
{
    if(rank <= [PlayingCard maxRank]){
        _rank = rank;
    }
}
@end
