//
//  ViewController.h
//  CardGameWithNoStoryBoard
//
//  Created by ByteDance on 2022/6/27.
//

#import <UIKit/UIKit.h>
#import "Deck.h"
@interface ViewController : UIViewController

- (instancetype) initWithCardCount:(NSUInteger)count  //初始化函数
                         usingDeck:(Deck *)deck;

- (void)chooseCardAtIndex:(NSUInteger) index;
- (Card *)cardAtIndex:(NSUInteger) index;

@property (nonatomic, readonly) NSInteger score;  //分数
@property (nonatomic, readonly) NSMutableString *detailString;

@end

