//
//  CardMachingGame.h
//  ios-app-01
//
//  Created by ByteDance on 2022/6/26.
//

#import <Foundation/Foundation.h>
#import "Deck.h"
NS_ASSUME_NONNULL_BEGIN

@interface CardMachingGame : NSObject

- (instancetype) initWithCardCount:(NSUInteger)count  //初始化函数
                         usingDeck:(Deck *)deck;

- (void)chooseCardAtIndex:(NSUInteger) index;
- (Card *)cardAtIndex:(NSUInteger) index;

@property (nonatomic, readonly) NSInteger score;  //分数
@property (nonatomic, readonly) NSMutableString *detailString;
@end
 
NS_ASSUME_NONNULL_END
