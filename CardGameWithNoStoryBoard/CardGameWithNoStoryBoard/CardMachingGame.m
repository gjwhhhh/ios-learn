//
//  CardMachingGame.m
//  ios-app-01
//
//  Created by ByteDance on 2022/6/26.
//

#import "CardMachingGame.h"
@interface CardMachingGame()
@property (nonatomic, readwrite) NSInteger score;
@property (nonatomic, strong) NSMutableArray *cards;
@property (nonatomic, readwrite) NSMutableString *detailString;
//@property (nonatomic) Deck * deck;
@end

@implementation CardMachingGame

- (NSMutableArray *)cards
{
    if(!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (instancetype)initWithCardCount:(NSUInteger)count
                        usingDeck:(Deck *)deck
{
    self = [super init];
    if(self){
        for(int i=0; i<count; i++){
            Card *card = [deck drawRandomCard];
            if(card){
                [self.cards addObject:card];
            }else{
                self = nil;
                break;
            }
        }
    }
    return self;
}

//- (Deck *)deck
//{
//    if(!_deck) _deck = [[PlayingCardDeck alloc ]init];
//    return _deck;
//}
static const int MATCH_BONUS = 4;
static const int MISMATH_PENALTY = 2;
static const int CONST_TO_CHOOSEN = 1;
//- (void)checkGameIsEnableSuccess  //检查是否可以正确的赢得游戏
//{
//    int IsNotMatchedCnt = 0;
//    NSMutableArray *cardTmps = [[NSMutableArray alloc]init];
//    for(Card *card in self.cards){
//        if(!card.isMatched){
//            IsNotMatchedCnt++;
//            [cardTmps addObject:card];
//        }
//        if(IsNotMatchedCnt > 2)
//            return ;
//    }
//    if(IsNotMatchedCnt==2 && cardTmps.count == 2){
//        int matchScore = [cardTmps[0] match:@[cardTmps[1]]];
//        if(!matchScore){
//            int cardIndex = [self.cards indexOfObject:cardTmps[0]];
//            Card *card = [self.deck drawRandomCard];
//            self.cards[cardIndex] = card;
//            NSLog(@"%@",card.contents);
//        }
//    }
//}
- (void)chooseCardAtIndex:(NSUInteger)index
{
    
    self.detailString = [NSMutableString stringWithFormat:@"原分数为: %d\n",self.score];
    Card *card = [self cardAtIndex:index];
    if(!card.isMatched){
        if(card.isChosen){
            card.chosen = NO;
        }else{
            for(Card *otherCard in self.cards){
                if(otherCard.isChosen && !otherCard.isMatched){
                    int matchScore = [card match:@[otherCard]];
                    if(matchScore){
                        self.score += matchScore * MATCH_BONUS;
                        otherCard.matched = YES;
                        card.matched = YES;
                        [self.detailString appendString:[NSString stringWithFormat:@"匹配得分 +%d*%d\n",matchScore,MATCH_BONUS] ];
                    }else{
                        self.score -= MISMATH_PENALTY;
                        otherCard.chosen = NO;
                        [self.detailString appendString:[NSString stringWithFormat:@"不匹配罚分 -%d\n",MISMATH_PENALTY]] ;
                    }
                    break;
                }
            }
            [self.detailString appendString:[NSString stringWithFormat:@"翻转牌消耗分数-%d\n",CONST_TO_CHOOSEN] ];
            self.score -= CONST_TO_CHOOSEN;
            card.chosen = YES;
           
        }
        [self.detailString appendString:[NSString stringWithFormat:@"操作完成后分数为: %d\n",self.score] ];
    }
    
//    [self checkGameIsEnableSuccess];
    
}

- (Card *)cardAtIndex:(NSUInteger)index
{
    return (index < [self.cards count]) ? (self.cards[index]) : nil;
}
- (instancetype)init
{
    return nil;
}
@end
