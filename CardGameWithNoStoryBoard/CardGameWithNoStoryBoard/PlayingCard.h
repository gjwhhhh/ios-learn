//
//  PlayingCard.h
//  ios-app-01
//
//  Created by ByteDance on 2022/6/26.
//

#import "Card.h"

NS_ASSUME_NONNULL_BEGIN
// 继承最简单的纸牌接口，实现游戏中纸牌的匹配逻辑
@interface PlayingCard : Card

@property (strong, nonatomic) NSString *suit;  //花色
@property (nonatomic) NSUInteger rank;    //纸牌Number

+ (NSArray *)validSuits;
+ (NSArray *)rankStrings;
+ (NSUInteger)maxRank;

@end

NS_ASSUME_NONNULL_END
