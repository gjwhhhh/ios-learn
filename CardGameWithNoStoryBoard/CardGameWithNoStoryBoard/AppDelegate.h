//
//  AppDelegate.h
//  CardGameWithNoStoryBoard
//
//  Created by ByteDance on 2022/6/27.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>{
   
}

@property (nonatomic, strong) UIWindow * window;
@property (nonatomic, strong) ViewController * viewController;

@end

