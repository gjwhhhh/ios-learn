//
//  ViewController.m
//  Attributor
//
//  Created by ByteDance on 2022/6/27.
//

#import "ViewController.h"
#import "TextStatsViewController.h"
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *body;
@property (weak, nonatomic) IBOutlet UILabel *headline;
@property (weak, nonatomic) IBOutlet UIButton *outlineButton;
@property (weak, nonatomic) IBOutlet UIButton *unoutlineButton;

@end

@implementation ViewController

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"AnalyzText"]){
        if([segue.destinationViewController isKindOfClass:[TextStatsViewController class]]){
            TextStatsViewController *tsvc = (TextStatsViewController *)segue.destinationViewController;
            tsvc.textToAnalyze = self.body.textStorage;
        }
    }
}

- (IBAction)changeBodySelectionColorToMatchBackgroundOfButton:(UIButton *)sender {
    [self.body.textStorage addAttribute:NSForegroundColorAttributeName value:sender.backgroundColor range:self.body.selectedRange];
}

- (void)viewDidLoad {  //初始化
    [super viewDidLoad];
//    NSLog(@"%@",self.body.text);
//    NSLog(@"%@",self.outlineButton.titleLabel.text)
//    // Do any additional setup after loading the view.
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:self.outlineButton.titleLabel.text];
    [title addAttributes:@{NSStrokeWidthAttributeName : @3,
                                          NSStrokeColorAttributeName:self.outlineButton.tintColor
                                        } range:NSMakeRange(0, [title length])];
    [self.outlineButton setAttributedTitle:title forState:UIControlStateNormal];
}
- (IBAction)outlineBodySelection:(id)sender {
    
    [self.body.textStorage addAttributes:@{NSStrokeWidthAttributeName : @-3,
                                          NSStrokeColorAttributeName:[UIColor blackColor]
                                        } range:self.body.selectedRange];
    
    
}
- (IBAction)unoutlineBodySelection:(id)sender {
    [self.body.textStorage removeAttribute:NSStrokeWidthAttributeName range:self.body.selectedRange];
}
/*
 这里实现了随系统字体大小变化的文本内容，使用通知的方式
 */
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //在通知中心注册
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(preferredFontsChanged:) name:UIContentSizeCategoryDidChangeNotification object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //在通知中心移除
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIContentSizeCategoryDidChangeNotification object:nil];
}

- (void)preferredFontsChanged:(NSNotification *)notification
{
    [self usePreferredFonts];
}
- (void)usePreferredFonts  //使用系统推荐字体大小
{
    self.body.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    self.headline.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
}
@end
