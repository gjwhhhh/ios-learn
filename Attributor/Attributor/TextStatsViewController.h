//
//  TextStatsViewController.h
//  Attributor
//
//  Created by ByteDance on 2022/7/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TextStatsViewController : UIViewController

@property(nonatomic,strong)NSAttributedString  *textToAnalyze;
@end

NS_ASSUME_NONNULL_END
