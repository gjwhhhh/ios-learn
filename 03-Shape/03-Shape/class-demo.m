//
//  class-demo.m
//  03-Shape
//
//  Created by ByteDance on 2022/6/23.
//

#import <Foundation/Foundation.h>

typedef enum{
    kCircle,
    kRectangle,
    kEgg
}ShapeType;


typedef enum{
    kRedColor,
    kGreenColor,
    kBlueColor
}ShapeColor;

typedef struct {
    int x, y, width, height;
}ShapeRect;

@interface Circle : NSObject
{
    @private
    ShapeColor fillColor;
    ShapeRect bounds;
}
- (void) setFillColor: (ShapeColor) fillColor;
- (void) setBounds: (ShapeRect) bounds;
- (void) draw;
@end  //Circle

NSString *colorName(ShapeColor colorName)
{
    switch(colorName){
        case kRedColor:
            return @"red";
            break;
        case kGreenColor:
            return @"green";
            break;
        case kBlueColor:
            return @"blue";
            break;
    }
    return @"no clue";
}

@implementation Circle

- (void)setFillColor:(ShapeColor)c
{
    fillColor = c;
} //setFillColor

- (void)setBounds:(ShapeRect)b
{
    bounds = b;
}
- (void) draw
{
    NSLog(@"drawing a circle at (%d %d %d %d) in %@",
          bounds.x, bounds.y,
          bounds.width, bounds.height,
          colorName(fillColor));
}
@end

int main(int argc, const char * argv[])
{
    id shapes[3];
    
    ShapeRect rect0 = {0, 0, 10, 30};
    shapes[0] = [Circle new];
    [shapes[0] setBounds: rect0];
    [shapes[0] setFillColor: kRedColor];
    [shapes[0] draw];
    
    return (0);
}
