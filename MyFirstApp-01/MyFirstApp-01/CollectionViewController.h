//
//  CollectionViewController.h
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CollectionViewController : UIViewController
{
    UICollectionView *_collectionView;
}
@end

NS_ASSUME_NONNULL_END
