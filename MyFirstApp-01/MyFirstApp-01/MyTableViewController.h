//
//  MyTableViewController.h
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyTableViewController : UIViewController<UITableViewDataSource,
UITableViewDelegate>


@property(nonatomic)UITableView *myTableView;
@property(nonatomic) NSMutableArray *myData;


@end

NS_ASSUME_NONNULL_END
