//
//  AppViewController.m
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/30.
//

#import "AppViewController.h"

@interface AppViewController ()
@property(strong,nonatomic) UILabel *label;
@property(strong,nonatomic)UINavigationBar *navigationBar;
@property(strong,nonatomic)UIView * userInfoView;
@property(strong,nonatomic)UIView * userTextView;
@property(strong,nonatomic)UIView * userView;
@property(strong,nonatomic)UIView * bookCollectionView;
@property(strong,nonatomic)NSMutableArray *imgArrays;
@property(strong,nonatomic)NSMutableArray *titleArrays;
@property(strong,nonatomic)NSMutableArray *authorArrays;
@end

@implementation AppViewController

- (NSMutableArray *)authorArrays
{
    NSArray *data = @[@"金庸",@"金庸"];
    return [NSMutableArray arrayWithArray:data];
}
- (NSMutableArray *)imgArrays
{
    NSArray *data = @[@"book",@"book"];
    return [NSMutableArray arrayWithArray:data];
}
- (NSMutableArray *)titleArrays
{
    NSArray *data = @[@"神雕侠侣（第二卷）新修版",@"神雕侠侣（第二卷）新修版"];
    return [NSMutableArray arrayWithArray:data];
}
- (void)back:(id)sender
{
}
- (void)detail:(id)sender
{
    UIAlertController *actionSheetController = [[UIAlertController alloc]init];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        NSLog(@"Tag 取消Button");
    }];
    UIAlertAction *reportAction = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        NSLog(@"Tag 举报Button");
    }];
    
    
    [actionSheetController addAction:reportAction];
    [actionSheetController addAction:cancelAction];
    
    actionSheetController.popoverPresentationController.sourceView = sender;
    [self presentViewController:actionSheetController animated:true completion:nil];
}


-(UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(size);
    // 绘制改变大小的图片
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}
//初始化导航栏
- (void)setUpNavigationBar
{
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat navigationBarHeight = 56;
    self.navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 30, screen.size.width, navigationBarHeight)];
    
    CGSize imgSize = CGSizeMake(20, 20);
    UIImage *backImage = [self scaleToSize:[UIImage imageNamed:@"back"] size:imgSize];
    UIBarButtonItem *backButtonItem  = [[UIBarButtonItem alloc] initWithImage:backImage style:UIBarButtonItemStyleDone target:self action:@selector(back:)];
    backButtonItem.tintColor = [UIColor blackColor];
    
    
    UIImage *detailImage = [self scaleToSize:[UIImage imageNamed:@"detail"] size:CGSizeMake(3, 16)];
    UIBarButtonItem *detailButtonItem  = [[UIBarButtonItem alloc] initWithImage:detailImage style:UIBarButtonItemStyleDone target:self action:@selector(detail:)];
    detailButtonItem.tintColor = [UIColor blackColor];
    
    
    UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"帖子详情"];
    navigationItem.leftBarButtonItem = backButtonItem;
    navigationItem.rightBarButtonItem = detailButtonItem;
    
    self.navigationBar.items = @[navigationItem];
    [self.view addSubview:self.navigationBar];
}

//作者信息
- (UIView *)setUpAuthor
{

    UIView *authorView = [[UIView alloc] init];
    authorView.backgroundColor = [UIColor whiteColor];
    
    return authorView;
    
}
//用户View
- (UIView *)setUpUserView
{
    const int marginLeft = 16;
    CGRect userImgRect = CGRectMake(0, 0, 32, 32);
    CGRect userLabelRect = CGRectMake(userImgRect.size.width+userImgRect.origin.x+marginLeft, 0, 100, 22);
    CGRect userTimeRect = CGRectMake(userImgRect.size.width+marginLeft, userLabelRect.size.height, 100, 17);
    CGRect iconRect = CGRectMake(0, userLabelRect.size.height+userTimeRect.size.height+3, 165, 29);
    
    UIView *userView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, userImgRect.size.height+userTimeRect.size.height+iconRect.size.height)];
    userView.translatesAutoresizingMaskIntoConstraints = NO;
    userView.backgroundColor = [UIColor whiteColor];
    //发布者头像
    UIImageView *userImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pubImg"]];
    [userImg setFrame:userImgRect];
    [userView addSubview:userImg];
    //发布者名称
    UILabel *userNameLabel = [[UILabel alloc]initWithFrame:userLabelRect];
    userNameLabel.textColor = [UIColor blackColor];
    userNameLabel.backgroundColor = [UIColor clearColor];
    userNameLabel.textAlignment = NSTextAlignmentLeft;
    userNameLabel.text = @"陈二狗";
    [userNameLabel setFont:[UIFont systemFontOfSize:16]];
    //时间
    UILabel *pubTimeLabel = [[UILabel alloc]initWithFrame:userTimeRect];
    pubTimeLabel.textColor = [UIColor blackColor];
    pubTimeLabel.backgroundColor = [UIColor clearColor];
    pubTimeLabel.textAlignment = NSTextAlignmentLeft;
    pubTimeLabel.text = @"2022年3月5日";
    UIFont *font = [UIFont systemFontOfSize:12];
    pubTimeLabel.alpha = 0.5;
    [pubTimeLabel setFont:font];
    
    UIView * iconView = [[UIView alloc]initWithFrame:iconRect];
    UILabel *iconFront = [[UILabel alloc]initWithFrame:CGRectMake(13, (iconRect.size.height-12)*1.0/2, 12, 12)];
    UILabel *iconText = [[UILabel alloc] initWithFrame:CGRectMake(iconFront.frame.size.width+13+6, (iconRect.size.height-17)*1.0/2, 144, 17)];
    iconText.textColor = [UIColor colorWithRed:255.0/255 green:95.0/255 blue:0 alpha:0.7];
    iconText.text = @"写一个关于春天浪漫的故事";
    [iconText setFont:[UIFont systemFontOfSize:10]];
    
    [iconFront setTextAlignment:NSTextAlignmentCenter];
    iconFront.text = @"#";
    iconFront.textColor = [UIColor colorWithRed:255.0/255 green:95.0/255 blue:0 alpha:0.7];
    [iconFront setFont:[UIFont systemFontOfSize:10]];
    iconFront.backgroundColor = [UIColor colorWithRed:255.0/255 green:95.0/255 blue:0 alpha:0.15];
    
    
    iconView.backgroundColor = [UIColor colorWithRed:255.0/255 green:249.0/255 blue:242.0/255 alpha:1];
    
    [iconView addSubview:iconText];
    [iconView addSubview:iconFront];
    
    [userView addSubview:iconView];
    [userView addSubview:userNameLabel];
    [userView addSubview:pubTimeLabel];
    
    return userView;
}
// 发布的文字内容
- (UIView *)setUpPubText
{
    UIView *textView = [[UIView alloc]init];
    
    textView.backgroundColor = [UIColor whiteColor];
    
    CGRect pubTextViewRect = CGRectMake(0, 0, 335, 72);
    
    UITextView * myTextView = [[UITextView alloc]initWithFrame:pubTextViewRect];
    
    textView.translatesAutoresizingMaskIntoConstraints = NO;
    myTextView.translatesAutoresizingMaskIntoConstraints = NO;
    [myTextView setText:@"少年听雨阁楼上，红烛昏罗帐。壮年听雨客大舟中，江阔云低、断雁叫西风。而今听雨僧庐下，鬓已星星也。悲欢离合总无情，一任阶前，点滴到天明。"];
    myTextView.delegate = self;

    [textView addSubview:myTextView];
    
    return textView;
}

# pragma mark - UICollectionView
//Cell的总数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

//每个UICollectionViewCell
// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BookUICollectionViewCell *cell=(BookUICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    int index = (int)indexPath.row;
    [cell setTitle:self.titleArrays[index] image:[UIImage imageNamed:self.imgArrays[index]] author:self.authorArrays[index] frame:CGRectMake(0, 0, 335, 84)];
//    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
//    cell.backgroundColor=[UIColor grayColor];
    return cell;
}

//每个Cell的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(335, 84);
}

- (UIView *)setUpBookCollectionView
{
    UIView *collectionView = [[UIView alloc]init];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    //行内块间距
    layout.minimumInteritemSpacing = 8;
    //行间距
    layout.minimumLineSpacing = 8;
    layout.itemSize = CGSizeMake(self.view.bounds.size.width, 50);
    //Collection header 的大小
    layout.headerReferenceSize = CGSizeMake(0,10);
    //排列方向
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    CGRect main = [[UIScreen mainScreen] bounds];
    UICollectionView * bookCollectionView;
//    int bookRectY = self.userTextView.frame.size.height + self.userInfoView.frame.size.height;
    bookCollectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, main.size.width-40, 200) collectionViewLayout:layout];
    
    [bookCollectionView setCollectionViewLayout:layout];
    [bookCollectionView setDataSource:self];
    [bookCollectionView setDelegate:self];

    [bookCollectionView registerClass:[BookUICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [bookCollectionView setBackgroundColor:[UIColor whiteColor]];
    
    [collectionView addSubview:bookCollectionView];
    return collectionView;
}

# pragma mark - viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNavigationBar];
    // Do any additional setup after loading the view.
    
    self.userView = [self setUpAuthor];
    [self.view addSubview:self.userView];
    
    self.userInfoView = [self setUpUserView];
    self.userTextView = [self setUpPubText];
    self.bookCollectionView = [self setUpBookCollectionView];
    
    [self.userView addSubview:self.userInfoView];
    [self.userView addSubview:self.userTextView];
    [self.userView addSubview:self.bookCollectionView];
    
    //userView的约束
    self.userView.translatesAutoresizingMaskIntoConstraints = NO;
    int paddingTop = self.navigationBar.bounds.size.height + self.navigationBar.frame.origin.y;
    UIEdgeInsets padding = UIEdgeInsetsMake(paddingTop, 20, 0, 20);
    
    self.userInfoView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.userView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(padding.top); //with is an optional semantic filler
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
//        make.bottom.equalTo().with.offset(20);
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
    //userInfoView的约束
    self.userInfoView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.userInfoView mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(self.userView.mas_top);
        make.width.equalTo(self.userTextView.mas_width);
        make.left.equalTo(self.userView.mas_left);
        make.right.equalTo(self.userView.mas_right);
    }];
    
//    self.userTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.userTextView.translatesAutoresizingMaskIntoConstraints = NO;
    //userTextView 的约束
    [self.userTextView mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(self.userInfoView.mas_bottom).with.offset(self.userInfoView.frame.size.height-10);
        make.width.equalTo(self.userView.mas_width);
        make.left.equalTo(self.userView.mas_left);
        make.right.equalTo(self.userView.mas_right);
    }];
    
    self.bookCollectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bookCollectionView mas_makeConstraints:^(MASConstraintMaker *make){
        make.bottom.equalTo(self.userView.mas_bottom).with.offset(0);
        make.top.equalTo(self.userTextView.mas_top).with.offset(70);
        make.width.equalTo(self.userView.mas_width);
        make.left.equalTo(self.userView.mas_left);
        make.right.equalTo(self.userView.mas_right);
    }];
     
//    self.userTextView.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.userTextView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.userInfoView attribute:NSLayoutAttributeTop multiplier:1.0 constant:self.userInfoView.frame.size.height]];
    //??????
//    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.userTextView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.userView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    
//    self.bookCollectionView.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.userView addConstraint:[NSLayoutConstraint constraintWithItem:self.bookCollectionView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.userTextView attribute:NSLayoutAttributeTop multiplier:1.0 constant:self.userTextView.frame.size.height]];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
