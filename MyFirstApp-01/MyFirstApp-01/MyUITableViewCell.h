//
//  MyUITableViewCell.h
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/30.
//

#import <UIKit/UIKit.h>

#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyUITableViewCell : UITableViewCell

@property(nonatomic) int index;

@property (nonatomic, weak) ViewController *VC;


@end

NS_ASSUME_NONNULL_END
    
