//
//  MyUICollectionViewCell.m
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/7/4.
//

#import "BookUICollectionViewCell.h"
@interface BookUICollectionViewCell()
{
    
}
@end

@implementation BookUICollectionViewCell


- (void)setTitle:(NSString *)title image:(UIImage *)image author:(NSString *)author frame:(CGRect)frame
{
    self.title = title;
    self.image = image;
    self.author = author;
    
    [self.contentView addSubview:[self setupView]];
}

- (UIView *)setupView
{
    self.backgroundColor = [UIColor colorWithRed:24.0/255 green:24.0/255 blue:24.0/255 alpha:0.03];
    
    CGRect imgRect = CGRectMake(0, 0, 60, 60);
    CGRect bookTitleRect = CGRectMake(imgRect.size.width+12, 5.5, 168, 20);
    CGRect authorRect = CGRectMake(imgRect.size.width+12, bookTitleRect.size.height+4, 24, 17);
    
    UIView *bookView = [[UIView alloc] initWithFrame:CGRectMake(12, 12, self.frame.size.width-24, self.frame.size.height-24)];
    UIImageView *image = [[UIImageView alloc]initWithImage:self.image];
    image.frame = imgRect;
    
    UILabel *bookTitleLabel = [[UILabel alloc]initWithFrame:bookTitleRect];
    bookTitleLabel.text = self.title;
    [bookTitleLabel setFont:[UIFont systemFontOfSize:12]];
    
    UILabel *authorLabel = [[UILabel alloc]initWithFrame:authorRect];
    authorLabel.text = self.author;
    [authorLabel setFont:[UIFont systemFontOfSize:10]];
    authorLabel.textColor = [UIColor colorWithRed:24.0/255 green:24.0/255 blue:24.0/255 alpha:0.5];
    
    [bookView addSubview:bookTitleLabel];
    [bookView addSubview:authorLabel];
    [bookView addSubview:image];
    return bookView;
}

@end
