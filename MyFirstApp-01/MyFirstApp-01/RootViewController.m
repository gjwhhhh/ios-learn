//
//  RootViewController.m
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/28.
//

#import "RootViewController.h"

@interface RootViewController ()
@property(nonatomic,strong) NSArray *data;
@end

@implementation RootViewController

- (NSArray *)data
{
    if(!_data)
        _data = @[@"1",@"2"];
    return _data;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.data count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    cell.textLabel.text = [@"Table View" stringByAppendingString:self.data[indexPath.row]];
    return cell;
}

- (void)loadView
{
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] bounds] style:UITableViewStylePlain];
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    tableView.delegate = self;
    tableView.dataSource = self;
    [tableView reloadData];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    self.view = tableView;
    
    
    /*
    CGRect titleRect = CGRectMake(0, 0, 300, 40);
       UILabel *tableTitle = [[UILabel alloc] initWithFrame:titleRect];
       tableTitle.textColor = [UIColor blueColor];
       tableTitle.backgroundColor = [tableView backgroundColor];
       tableTitle.opaque = YES;
       tableTitle.font = [UIFont boldSystemFontOfSize:18];
       tableTitle.text = @"title";
//       tableView.tableHeaderView = tableTitle;
//       [tableView reloadData];
   */

}
  

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}


@end
