//
//  startViewController.m
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/7/4.
//

#import "startViewController.h"

@interface startViewController ()
@property(strong,nonatomic)UINavigationBar *navigationBar;
@end

@implementation startViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNavigationBar];
    CGRect main = [[UIScreen mainScreen]bounds];

    UIView *userView = [self setUpUserView];
    [self.cellViewData addObject:userView];

    UIView *rootView = [self setUpBookCollectionView];
    rootView.frame = CGRectMake(0, self.navigationBar.frame.origin.y+self.navigationBar.frame.size.height, main.size.width, main.size.height-20);


    [self.view addSubview:rootView];
    
    // This allocates a label
    UILabel *prefixLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    //This sets the label text
    prefixLabel.text =@"## ";
    // This sets the font for the label
    [prefixLabel setFont:[UIFont boldSystemFontOfSize:14]];
    // This fits the frame to size of the text
    [prefixLabel sizeToFit];

    // This allocates the textfield and sets its frame
    UITextField *textField = [[UITextField  alloc] initWithFrame:
    CGRectMake(20, 200, 280, 30)];

    // This sets the border style of the text field
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.contentVerticalAlignment =
    UIControlContentVerticalAlignmentCenter;
    [textField setFont:[UIFont boldSystemFontOfSize:12]];

    //Placeholder text is displayed when no text is typed
    textField.placeholder = @"Simple Text field";

    //Prefix label is set as left view and the text starts after that
    textField.leftView = prefixLabel;

    //It set when the left prefixLabel to be displayed
    textField.leftViewMode = UITextFieldViewModeAlways;

    // Adds the textField to the view.
    [self.view addSubview:textField];

    // sets the delegate to the current class
    textField.delegate = self;
}
#pragma mark - TextField Delegates

// This method is called once we click inside the textField
-(void)textFieldDidBeginEditing:(UITextField *)textField{
   NSLog(@"Text field did begin editing");
}

// This method is called once we complete editing
-(void)textFieldDidEndEditing:(UITextField *)textField{
   NSLog(@"Text field ended editing");
}

// This method enables or disables the processing of return key
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)viewDidUnload {
//   label = nil;
   [super viewDidUnload];
}

- (void)back:(id)sender
{
}
- (void)detail:(id)sender
{
    UIAlertController *actionSheetController = [[UIAlertController alloc]init];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        NSLog(@"Tag 取消Button");
    }];
    UIAlertAction *reportAction = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        NSLog(@"Tag 举报Button");
    }];
    
    
    [actionSheetController addAction:reportAction];
    [actionSheetController addAction:cancelAction];
    
    actionSheetController.popoverPresentationController.sourceView = sender;
    [self presentViewController:actionSheetController animated:true completion:nil];
}

# pragma mark ViewCreate

-(UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(size);
    // 绘制改变大小的图片
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}
//初始化导航栏
- (void)setUpNavigationBar
{
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat navigationBarHeight = 56;
    self.navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 30, screen.size.width, navigationBarHeight)];
    
    CGSize imgSize = CGSizeMake(20, 20);
    UIImage *backImage = [self scaleToSize:[UIImage imageNamed:@"back"] size:imgSize];
    UIBarButtonItem *backButtonItem  = [[UIBarButtonItem alloc] initWithImage:backImage style:UIBarButtonItemStyleDone target:self action:@selector(back:)];
    backButtonItem.tintColor = [UIColor blackColor];
    
    UIImage *detailImage = [self scaleToSize:[UIImage imageNamed:@"detail"] size:CGSizeMake(3, 16)];
    UIBarButtonItem *detailButtonItem  = [[UIBarButtonItem alloc] initWithImage:detailImage style:UIBarButtonItemStyleDone target:self action:@selector(detail:)];
    detailButtonItem.tintColor = [UIColor blackColor];
    
    UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"帖子详情"];
    navigationItem.leftBarButtonItem = backButtonItem;
    navigationItem.rightBarButtonItem = detailButtonItem;
    
    self.navigationBar.items = @[navigationItem];
    [self.view addSubview:self.navigationBar];
}

//用户View
- (UIView *)setUpUserView
{
    const int marginLeft = 16;
    CGRect userImgRect = CGRectMake(0, 0, 32, 32);
    CGRect userLabelRect = CGRectMake(userImgRect.size.width+userImgRect.origin.x+marginLeft, 0, 100, 22);
    CGRect userTimeRect = CGRectMake(userImgRect.size.width+marginLeft, userLabelRect.size.height, 100, 17);
    CGRect iconRect = CGRectMake(0, userLabelRect.size.height+userTimeRect.size.height+3, 165, 29);
    
    UIView *userView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, userImgRect.size.height+userTimeRect.size.height+iconRect.size.height)];
    userView.translatesAutoresizingMaskIntoConstraints = NO;
    userView.backgroundColor = [UIColor whiteColor];
    //发布者头像
    UIImageView *userImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pubImg"]];
    [userImg setFrame:userImgRect];
    [userView addSubview:userImg];
    //发布者名称
    UILabel *userNameLabel = [[UILabel alloc]initWithFrame:userLabelRect];
    userNameLabel.textColor = [UIColor blackColor];
    userNameLabel.backgroundColor = [UIColor clearColor];
    userNameLabel.textAlignment = NSTextAlignmentLeft;
    userNameLabel.text = @"陈二狗";
    [userNameLabel setFont:[UIFont systemFontOfSize:16]];
    //时间
    UILabel *pubTimeLabel = [[UILabel alloc]initWithFrame:userTimeRect];
    pubTimeLabel.textColor = [UIColor blackColor];
    pubTimeLabel.backgroundColor = [UIColor clearColor];
    pubTimeLabel.textAlignment = NSTextAlignmentLeft;
    pubTimeLabel.text = @"2022年3月5日";
    UIFont *font = [UIFont systemFontOfSize:12];
    pubTimeLabel.alpha = 0.5;
    [pubTimeLabel setFont:font];
    
    UIView * iconView = [[UIView alloc]initWithFrame:iconRect];
    UILabel *iconFront = [[UILabel alloc]initWithFrame:CGRectMake(13, (iconRect.size.height-12)*1.0/2, 12, 12)];
    UILabel *iconText = [[UILabel alloc] initWithFrame:CGRectMake(iconFront.frame.size.width+13+6, (iconRect.size.height-17)*1.0/2, 144, 17)];
    iconText.textColor = [UIColor colorWithRed:255.0/255 green:95.0/255 blue:0 alpha:0.7];
    iconText.text = @"写一个关于春天浪漫的故事";
    [iconText setFont:[UIFont systemFontOfSize:10]];
    
    [iconFront setTextAlignment:NSTextAlignmentCenter];
    iconFront.text = @"#";
    iconFront.textColor = [UIColor colorWithRed:255.0/255 green:95.0/255 blue:0 alpha:0.7];
    [iconFront setFont:[UIFont systemFontOfSize:10]];
    iconFront.backgroundColor = [UIColor colorWithRed:255.0/255 green:95.0/255 blue:0 alpha:0.15];
    
    
    iconView.backgroundColor = [UIColor colorWithRed:255.0/255 green:249.0/255 blue:242.0/255 alpha:1];
    
    [iconView addSubview:iconText];
    [iconView addSubview:iconFront];
    
    [userView addSubview:iconView];
    [userView addSubview:userNameLabel];
    [userView addSubview:pubTimeLabel];
    
    return userView;
}

#pragma mark UICellectionViewCell
- (UIView *)setUpBookCollectionView
{
    UIView *collectionView = [[UIView alloc]init];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    //行内块间距
    layout.minimumInteritemSpacing = 8;
    //行间距
    layout.minimumLineSpacing = 8;
    layout.itemSize = CGSizeMake(self.view.bounds.size.width, 50);
    //Collection header 的大小
    layout.headerReferenceSize = CGSizeMake(0,10);
    //排列方向
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    CGRect main = [[UIScreen mainScreen] bounds];
    UICollectionView * bookCollectionView;
//    int bookRectY = self.userTextView.frame.size.height + self.userInfoView.frame.size.height;
    bookCollectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, main.size.width-40, 200) collectionViewLayout:layout];
    
    [bookCollectionView setCollectionViewLayout:layout];
    [bookCollectionView setDataSource:self];
    [bookCollectionView setDelegate:self];

    [bookCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [bookCollectionView setBackgroundColor:[UIColor whiteColor]];
    
    self.rootCollectionView = bookCollectionView;
    
    [collectionView addSubview:bookCollectionView];
    return collectionView;
}
//Cell的总数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}

//每个UICollectionViewCell
// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
//    int index = (int)indexPath.row;
//    [cell.contentView addSubview:self.cellViewData[index]];
    cell.backgroundColor=[UIColor greenColor];
    return cell;
}

//每个Cell的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect main = [[UIScreen mainScreen]bounds];
    return CGSizeMake(335, 84);
//    return CGSizeMake(main.size.width, 84);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
