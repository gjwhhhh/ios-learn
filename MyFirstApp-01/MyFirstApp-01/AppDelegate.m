//
//  AppDelegate.m
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/25.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "MyTableViewController.h"
@interface AppDelegate ()

@end


@implementation AppDelegate

#pragma mark - 应用委托对象的回调方法
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
//    self.window=[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];//创建一个Window
//    self.window.backgroundColor=[UIColor whiteColor]; //
//
//    UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:[[testViewController alloc] init]];
//    nav.navigationBar.hidden = YES;
//    self.window.rootViewController=nav; //设置好根视图控制器
//    [self.window makeKeyAndVisible]; //设置这个window为主(key)窗口并设置成为可见
    self.window = [UIWindow new];
    self.window.backgroundColor=[UIColor whiteColor];
    self.window.rootViewController = [AppViewController new];
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
