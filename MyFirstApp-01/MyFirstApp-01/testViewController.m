//
//  testViewController.m
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/7/4.
//

#import "testViewController.h"

@interface testViewController ()

@end

@implementation testViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    BookUICollectionViewCell *book = [[BookUICollectionViewCell alloc]init];
    [book setTitle:@"神雕侠侣（第二卷）新修版" image:[UIImage imageNamed:@"book"] author:@"金庸" frame:CGRectMake(20, 80, 335, 84)];
    
    book.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(event:)];
    [book addGestureRecognizer:tapGesture];
    [tapGesture setNumberOfTapsRequired:1];
    
    [self.view addSubview:book];
}

- (void)event:(UITapGestureRecognizer *)gesture
{
//    UINavigationController *nav = (UINavigationController *)[
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
