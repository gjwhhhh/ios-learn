//
//  MyUITableViewCell.m
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/30.
//

#import "MyUITableViewCell.h"


@implementation MyUITableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIButton *btn = [self createButton];
        [self.contentView addSubview:btn];
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (UIButton *)createButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(100, 10, 30, 30);
    [button setBackgroundImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

- (void)delete {
    [self.VC deleteCell:self.tag];
    
}

@end
