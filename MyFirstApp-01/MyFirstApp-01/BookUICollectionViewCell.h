//
//  MyUICollectionViewCell.h
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/7/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BookUICollectionViewCell : UICollectionViewCell
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *author;
@property(nonatomic,strong) UIImage *image;

- (void)setTitle:(NSString *)title image:(UIImage *)image author:(NSString *)author frame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
