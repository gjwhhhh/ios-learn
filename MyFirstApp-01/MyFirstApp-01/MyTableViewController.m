//
//  MyTableViewController.m
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/30.
//

#import "MyTableViewController.h"
/*
 使用section1 section2 的UITableView
 */
@interface MyTableViewController ()

@end

@implementation MyTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // table view data is being set here
    self.myData = [[NSMutableArray alloc]initWithObjects:
    @"Data 1 in array",@"Data 2 in array",@"Data 3 in array",
    @"Data 4 in array",@"Data 5 in array",@"Data 5 in array",
    @"Data 6 in array",@"Data 7 in array",@"Data 8 in array",
    @"Data 9 in array", nil];
    
    CGRect winFrame = [[UIScreen mainScreen]bounds];
    CGRect tableFrame = CGRectMake(0, 80, winFrame.size.width, winFrame.size.height-100);
    self.myTableView = [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
    self.myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;
    //需要创建新的单元格时，告诉UITableView对象要实例化哪个类
    [self.myTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    
    // Do any additional setup after loading the view, typically from a nib.
    [self.view addSubview:self.myTableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:
  (NSInteger)section{
    return [self.myData count]/2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:
  (NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
    cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:
        UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSString *stringForCell;
    if (indexPath.section == 0) {
        stringForCell= [self.myData objectAtIndex:indexPath.row];

    }
    else if (indexPath.section == 1){
        stringForCell= [self.myData objectAtIndex:indexPath.row+ [self.myData count]/2];

    }
    [cell.textLabel setText:stringForCell];
    return cell;
}

// Default is 1 if not implemented
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:
  (NSInteger)section{
    NSString *headerTitle;
    if (section==0) {
        headerTitle = @"Section 1 Header";
    }
    else{
        headerTitle = @"Section 2 Header";

    }
    return headerTitle;
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:
  (NSInteger)section{
    NSString *footerTitle;
    if (section==0) {
        footerTitle = @"Section 1 Footer";
    }
    else{
        footerTitle = @"Section 2 Footer";
        
    }
    return footerTitle;
}

#pragma mark - TableView delegate

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:
// (NSIndexPath *)indexPath{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    NSLog(@"Section:%d Row:%d selected and its data is %@",
//    (int)indexPath.section,indexPath.row,cell.textLabel.text);
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        NSLog(@"Section:%d Row:%d selected and its data is %@",
        indexPath.section,indexPath.row,cell.textLabel.text);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
