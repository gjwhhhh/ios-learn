//
//  RootViewController.h
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RootViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSArray *timeZoneNames;
@end

NS_ASSUME_NONNULL_END
