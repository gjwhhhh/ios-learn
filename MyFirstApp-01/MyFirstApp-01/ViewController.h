//
//  ViewController.h
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/29.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
//声明辅助函数，该函数返回特定文件的路径，用于保存用户的任务列表信息
NSString *docPath(void);

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIApplicationDelegate>

@property(nonatomic)UITableView *taskTable;
@property(nonatomic)UITextField *taskField;
@property(nonatomic)UIButton    *insertButton;
@property(nonatomic)NSMutableArray *tasks;


- (void)addTasks:(id)sender;
- (void)deleteCell:(NSUInteger)index;

@end

NS_ASSUME_NONNULL_END
