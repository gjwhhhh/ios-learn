//
//  startViewController.h
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/7/4.
//

#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface startViewController : ViewController

@property(nonatomic,strong) UICollectionView *rootCollectionView;
@property(nonatomic,strong) NSMutableArray *cellViewData;
@end

NS_ASSUME_NONNULL_END
