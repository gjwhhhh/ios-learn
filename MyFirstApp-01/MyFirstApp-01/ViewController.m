//
//  ViewController.m
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/6/29.
//

#import "ViewController.h"


#import "MyUITableViewCell.h"
@interface ViewController ()
@property(nonatomic,strong) NSMutableArray *closeButtons;
@end

NSString *docPath()
{
    NSArray *pathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [pathList[0] stringByAppendingPathComponent:@"data.td"];
}


@implementation ViewController




-(NSMutableArray *)closeButtons
{
    if(!_closeButtons)
        _closeButtons = [NSMutableArray array];
    return _closeButtons;
}

#pragma mark - CreateUI
#pragma mark - Actions
- (void)addTasks:(id)sender
{
    NSString *text = [self.taskField text];
    if([text length]==0){
        return ;
    }
    //任务添加到tasks数组
    [self.tasks addObject:text];
    //刷新表格视图，显示新加入的任务
    [self.taskTable reloadData];
    
    //清空内容
    [self.taskField setText:@""];
    //关闭键盘
    [self.taskField resignFirstResponder];
}

- (void)deleteCell:(NSUInteger)index
{
    [self.tasks removeObjectAtIndex:index];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.taskTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    //删除完reloadData
    [self.taskTable reloadData];
}

#pragma mark - 管理表格视图
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.tasks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //检查是否有可以重用的UITableViewCell对象
    MyUITableViewCell *cell = (MyUITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
      
    NSString *item = [self.tasks objectAtIndex:indexPath.row];
    cell.textLabel.text = item;
    cell.tag = (int)indexPath.row;
    cell.index = (int)indexPath.row;
    cell.VC = self;
    
    return cell;
}


- (void)viewDidLoad {
    [super viewDidLoad];
//    //消息通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];

    NSArray *plist = [NSArray arrayWithContentsOfFile:docPath()];
    if(plist!=nil){ //如果数组存在就拷贝到tasks
        self.tasks = [plist mutableCopy];
    }else{
        self.tasks = [NSMutableArray array];
    }

    CGRect winFrame = [[UIScreen mainScreen]bounds];
    CGRect tableFrame = CGRectMake(0, 80, winFrame.size.width, winFrame.size.height-100);
    CGRect fieldFrame = CGRectMake(20, 40, 200, 31);
    CGRect buttonFrame = CGRectMake(228, 40, 72, 31);

    self.taskTable = [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
    self.taskTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.taskTable.dataSource = self;
    self.taskTable.delegate = self;
    //需要创建新的单元格时，告诉UITableView对象要实例化哪个类
    [self.taskTable registerClass:[MyUITableViewCell class] forCellReuseIdentifier:@"Cell"];

    //创建并设置UITextField对象，以便用户输入内容并按下UIButton时，将新的单元格插入UITableView
    self.taskField = [[UITextField alloc] initWithFrame:fieldFrame];
    self.taskField.borderStyle = UITextBorderStyleRoundedRect;
    self.taskField.placeholder = @"Type a task, tap Insert";

    //创建并设置UIButton对象
    self.insertButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.insertButton.frame = buttonFrame;
    [self.insertButton setTitle:@"Insert" forState:UIControlStateNormal];
    [self.insertButton addTarget:self action:@selector(addTasks:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.taskTable];
    [self.view addSubview:self.taskField];
    [self.view addSubview:self.insertButton];
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"!!!applicationDidEnterBackground");
    [self.tasks writeToFile:docPath() atomically:YES];
}
- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"!!!applicationWillTerminate");
    [self.tasks writeToFile:docPath() atomically:YES];
}


@end
