//
//  MyProtocol.h
//  MVC-storyBoard
//
//  Created by ByteDance on 2022/7/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MyProtocol

@optinoal
- (void)methodOptional:(NNString *)stringParams;

@end

@interface MyClass : NSObject

@end

NS_ASSUME_NONNULL_END
