//
//  PlayCardGameController.m
//  ios-app-01
//
//  Created by ByteDance on 2022/6/27.
//

#import "PlayCardGameController.h"
#import "PlayingCardDeck.h"
@interface PlayCardGameController ()
@property(nonatomic)UITextField *taskField;
@end

@implementation PlayCardGameController

- (Deck *)createDeck
{
    return [[PlayingCardDeck alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect fieldFrame = CGRectMake(20, 40, 200, 31);
    // Do any additional setup after loading the view.
    self.taskField = [[UITextField alloc] initWithFrame:fieldFrame];
    self.taskField.borderStyle = UITextBorderStyleRoundedRect;
    self.taskField.placeholder = @"Type a task, tap Insert";
    [self.taskField setEnabled:YES];
    [self.view addSubview:self.taskField];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
