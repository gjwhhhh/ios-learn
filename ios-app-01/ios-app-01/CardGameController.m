//
//  ViewController.m
//  ios-app-01
//
//  Created by ByteDance on 2022/6/25.
//

#import "CardGameController.h"
#import "Deck.h"
#import "PlayingCardDeck.h"
#import "CardMachingGame.h"


@interface CardGameController ()
@property (nonatomic, strong) Deck *deck;
@property (nonatomic, strong) CardMachingGame *game;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
@end

@implementation CardGameController
- (CardMachingGame *)game
{
    if(!_game) _game = [[CardMachingGame alloc] initWithCardCount:[self.cardButtons count] usingDeck:[self createDeck]];
    return _game;
}

- (Deck *)createDeck  //创建牌堆 abstract
{
    return nil;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)touchCardButton:(UIButton *)sender {
    
    int cardIndex = [self.cardButtons indexOfObject:sender];
    
    [self.game chooseCardAtIndex:cardIndex];
    [self updateUI];
}
- (void)updateUI
{
    for(UIButton *cardButton in self.cardButtons){
        int cardIndex = [self.cardButtons indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardIndex];
        [cardButton setTitle:[self titleForCard:card] forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self backgroundImageForCard:card] forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
        }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d",self.game.score];
    self.detailTextView.text = self.game.detailString;
    
      
}

- (NSString *)titleForCard:(Card *)card
{
    return card.isChosen ? card.contents : @"";
}

- (UIImage *)backgroundImageForCard:(Card *)card
{
    return [UIImage imageNamed:card.isChosen ? @"front" : @"back"];
}
@end
