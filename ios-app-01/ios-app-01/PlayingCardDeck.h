//
//  PlayingCardDeck.h
//  ios-app-01
//
//  Created by ByteDance on 2022/6/26.
//

#import "Deck.h"

NS_ASSUME_NONNULL_BEGIN
//继承牌堆并实现游戏中牌堆的方法
@interface PlayingCardDeck : Deck

@end

NS_ASSUME_NONNULL_END
