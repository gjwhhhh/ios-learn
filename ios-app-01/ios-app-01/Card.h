//
//  Card.h
//  ios-app-01
//
//  Created by ByteDance on 2022/6/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Card : NSObject

@property (strong,nonatomic) NSString *contents;  //纸牌的内容
@property (nonatomic, getter=isChosen) BOOL chosen;  
@property (nonatomic, getter=isMatched) BOOL matched;

- (int)match:(NSArray *)otherCards;

@end

NS_ASSUME_NONNULL_END
