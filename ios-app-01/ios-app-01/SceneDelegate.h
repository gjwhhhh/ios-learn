//
//  SceneDelegate.h
//  ios-app-01
//
//  Created by ByteDance on 2022/6/25.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

