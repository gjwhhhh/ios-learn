//
//  ViewController.h
//  ios-app-01
//
//  Created by ByteDance on 2022/6/25.
//
// Abstract class. 抽象类，必须实现下面的方法
#import <UIKit/UIKit.h>
#import "Deck.h"

@interface CardGameController : UIViewController
- (Deck *)createDeck;  // abstract

@end

