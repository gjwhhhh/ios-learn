//
//  main.m
//  28-Block
//
//  Created by ByteDance on 2022/6/29.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSArray *originalStrings = @[@"Sauerkraut",@"Raygun",@"BigNerdRanch",@"Missiisi"];
        NSLog(@"orginal strings : %@",originalStrings);
        NSMutableArray * newStrings = [NSMutableArray array];
        NSArray *vowels = @[@"a",@"e",@"i",@"o",@"u"];
        
        //声明Block变量
        void (^devowelizer)(id, NSUInteger, BOOL *);
        //将Block对象赋给变量
        devowelizer = ^(id string, NSUInteger i, BOOL *stop){
            NSMutableString *newString = [NSMutableString stringWithString:string];
            for(NSString *s in vowels){
                NSRange fullRange = NSMakeRange(0, [newString length]);
                [newString replaceOccurrencesOfString:s withString:@"" options:NSCaseInsensitiveSearch range:fullRange];
            }
            [newStrings addObject:newString];
        };
        
        [originalStrings enumerateObjectsUsingBlock:devowelizer];
        NSLog(@"new strings: %@",newStrings);
    }
    return 0;
}
