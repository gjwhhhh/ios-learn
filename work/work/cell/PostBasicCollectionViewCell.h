//
//  SSPostBasicCollectionViewCell.h
//  work
//
//  Created by ByteDance on 2022/7/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PostBasicCollectionViewCell : UICollectionViewCell

- (void)setupWithObject:(id)obj;

@end

NS_ASSUME_NONNULL_END
