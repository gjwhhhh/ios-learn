//
//  MyUICollectionViewCell.h
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/7/4.
//

#import "PostBasicCollectionViewCell.h"
#import "Post.h"
#import "SDWebImage/SDWebImage.h"
#import "ViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface PostBookCollectionViewCell : PostBasicCollectionViewCell
@property(nonatomic,weak)ViewController *viewController;
+ (double)viewCellHeight;

@end

NS_ASSUME_NONNULL_END
