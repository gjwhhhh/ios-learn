//
//  PostCommentListCollectionViewCell.h
//  work
//
//  Created by ByteDance on 2022/7/6.
//

#import "PostBasicCollectionViewCell.h"
#import "Comment.h"
#import "VariableLabel.h"
#import "Masonry.h"
#import "ViewController.h"
#import "Lottie.h"
#import "InsideButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface PostCommentCollectionViewCell : PostBasicCollectionViewCell
@property(nonatomic) int index;
@property(nonatomic,weak) ViewController *viewcontroller;
+ (double)viewHeight;

@end

NS_ASSUME_NONNULL_END
