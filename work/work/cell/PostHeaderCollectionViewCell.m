//
//  SSPostHeaderCollectionViewCell.m
//  work
//
//  Created by ByteDance on 2022/7/4.
//

#import "PostHeaderCollectionViewCell.h"

@interface PostHeaderCollectionViewCell ()
{

}

@property (nonatomic, strong) UIImageView *avatarImage;
@property (nonatomic, strong) UIImage *uiImage;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) UILabel *postTimeLabel;
@property (nonatomic ,strong) UIView *titleView;

@property(nonatomic) CGRect userImgRect,userLabelRect,userTimeRect,iconRect;
@property(nonatomic)UILabel *iconFront, *iconText;

@end

@implementation PostHeaderCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}
+(double)viewCellHeight
{
    const int marginLeft = 16;
    const int marginTop = 3;
    CGRect userImgRect = CGRectMake(0, 19, 32, 32);
    CGRect userLabelRect = CGRectMake(userImgRect.size.width+userImgRect.origin.x+marginLeft, 19, 100, 22);
    CGRect userTimeRect =CGRectMake(userImgRect.size.width+marginLeft, userLabelRect.origin.y+userLabelRect.size.height, 100, 17);
    CGRect iconRect =  CGRectMake(0, userLabelRect.origin.y+userLabelRect.size.height+userTimeRect.size.height+3, 210, 29);
    return userLabelRect.size.height + userTimeRect.size.height + iconRect.size.height+marginTop + 19;
}

- (void)setupRect
{
    const int marginLeft = 16;
    self.userImgRect = CGRectMake(0, 19, 32, 32);
    self.userLabelRect = CGRectMake(self.userImgRect.size.width+self.userImgRect.origin.x+marginLeft, 19, 100, 22);
    self.userTimeRect = CGRectMake(self.userImgRect.size.width+marginLeft, self.userLabelRect.origin.y+self.userLabelRect.size.height, 100, 17);
    self.iconRect = CGRectMake(0, self.userLabelRect.origin.y+self.userLabelRect.size.height+self.userTimeRect.size.height+3, 210, 29);
    
}
#pragma mark -- setupUI
- (void)setupUI {
    [self setupRect];
    // init avatar
    [self setupAvatarImgWithFrame:self.userImgRect];
    // init nickname
    [self setupNickNameLabelWithFrame:self.userLabelRect];
    // init posttime
    [self setupPostTimeLabelWithFrame:self.userTimeRect];
    // init title
    [self setupTitleViewWithFrame: self.iconRect];
    
    //add view to self.contentView
    [self.contentView addSubview:self.titleView];
    [self.contentView addSubview:self.userNameLabel];
    [self.contentView addSubview:self.avatarImage];
    [self.contentView addSubview:self.postTimeLabel];
    
    [self titleViewAutoLayout];
}
- (void)titleViewAutoLayout
{
    //titleView布局约束
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(self.postTimeLabel.mas_bottom).with.offset(3);
        make.left.equalTo(self.mas_left);
        make.height.mas_equalTo(self.iconRect.size.height);
        make.right.equalTo(self.iconText.mas_right).with.offset(10);
    }];
   
}
- (void)setupTitleViewWithFrame:(CGRect)frame
{
    self.titleView = [[UIView alloc]initWithFrame:frame];
   
    self.iconFront = [[UILabel alloc]initWithFrame:CGRectMake(13, (frame.size.height-12)*1.0/2, 12, 12)];
    
    self.iconText = [[UILabel alloc]init];
    self.iconText.textColor = [UIColor colorWithRed:255.0/255 green:95.0/255 blue:0 alpha:0.7];
    self.iconText.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.iconText setFont:[UIFont systemFontOfSize:10]];
    
    [self.iconFront setTextAlignment:NSTextAlignmentCenter];
    self.iconFront.text = @"#";
    self.iconFront.textColor = [UIColor colorWithRed:255.0/255 green:95.0/255 blue:0 alpha:0.7];
    [self.iconFront setFont:[UIFont systemFontOfSize:10]];
    self.iconFront.backgroundColor = [UIColor colorWithRed:255.0/255 green:95.0/255 blue:0 alpha:0.15];
    self.iconFront.layer.cornerRadius = 8.0f;
    self.iconFront.layer.masksToBounds = YES;
        
    self.titleView.layer.cornerRadius = 6;
    self.titleView.layer.masksToBounds = YES;
    self.titleView.backgroundColor = [UIColor colorWithRed:255.0/255 green:249.0/255 blue:242.0/255 alpha:1];
    
    [self.titleView addSubview:self.iconText];
    [self.titleView addSubview:self.iconFront];
    
    UITapGestureRecognizer*tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
    [self.titleView addGestureRecognizer:tapGestureRecognizer];
}

- (void)click
{
//    NewViewController *cover = [[NewViewController alloc] init];
//    [self.viewController.navigationController pushViewController:cover animated:YES];
    //初始化
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"点击了帖子标签" message:@"" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    //显示alertView
    [alert show];
}


- (void)setupPostTimeLabelWithFrame:(CGRect)frame
{
    //时间
    self.postTimeLabel = [[UILabel alloc]initWithFrame:frame];
    self.postTimeLabel.textColor = [UIColor blackColor];
    self.postTimeLabel.backgroundColor = [UIColor clearColor];
    self.postTimeLabel.textAlignment = NSTextAlignmentLeft;
    UIFont *font = [UIFont systemFontOfSize:12];
    self.postTimeLabel.alpha = 0.5;
    [self.postTimeLabel setFont:font];
}

- (void)setupAvatarImgWithFrame:(CGRect)frame
{
    self.avatarImage = [[UIImageView alloc] init];
    [self.avatarImage setFrame:frame];
    self.avatarImage.layer.cornerRadius = CGRectGetWidth(self.avatarImage.bounds)/2.f;                  
    self.avatarImage.clipsToBounds = YES;
}
- (void)setupNickNameLabelWithFrame:(CGRect)frame
{
    self.userNameLabel = [[UILabel alloc]initWithFrame:frame];
    self.userNameLabel.textColor = [UIColor blackColor];
    self.userNameLabel.backgroundColor = [UIColor clearColor];
    self.userNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.userNameLabel setFont:[UIFont systemFontOfSize:16]];
    
}

- (void)setupWithObject:(id)obj {
    [self setupWithpost:obj];
}

- (void)setupWithpost:(Post *)post{
    self.userNameLabel.text = post.userInfo.userName;
    
    [self.avatarImage sd_setImageWithURL:[NSURL URLWithString:post.userInfo.avatar]
                      placeholderImage:[UIImage imageNamed:@"pubImg"]];
    
//    [self.avatarImage sd_setImageWithURL:[NSURL URLWithString:@"https://profile-avatar.csdnimg.cn/default.jpg!1"]];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    NSString *date = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:post.createTime]];
    self.postTimeLabel.text = date;
    self.iconText.text = post.title;
    
    [self labelsizeToFit];
}

- (void)labelsizeToFit
{
    CGSize size = [self.iconText sizeThatFits:CGSizeMake(210, 17)];
    double marginLeft = 6;
    double iconTextX = self.iconFront.frame.size.width+self.iconFront.frame.origin.x+marginLeft;
    self.iconText.frame = CGRectMake(iconTextX, (self.titleView.frame.size.height-size.height)*1.0/2, size.width, size.height);
}
@end
