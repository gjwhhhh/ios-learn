//
//  SSPostHeaderCollectionViewCell.h
//  work
//
//  Created by ByteDance on 2022/7/4.
//

#import "PostBasicCollectionViewCell.h"
#import "UserInfo.h"
#import "Post.h"
#import "SDWebImage/SDWebImage.h"
#import "Masonry.h"
#import "ViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface PostHeaderCollectionViewCell : PostBasicCollectionViewCell
@property(nonatomic,weak) ViewController *viewController;
- (void)setupWithpost:(Post *)post;
+ (double)viewCellHeight; 
@end

NS_ASSUME_NONNULL_END
