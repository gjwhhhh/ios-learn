//
//  SSPostContentCollectionViewCell.h
//  work
//
//  Created by ByteDance on 2022/7/4.
//

#import "PostBasicCollectionViewCell.h"
#import "Post.h"
#import "VariableLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PostContentCollectionViewCell : PostBasicCollectionViewCell

- (void)setupWithPost:(Post *)post;

@end

NS_ASSUME_NONNULL_END
