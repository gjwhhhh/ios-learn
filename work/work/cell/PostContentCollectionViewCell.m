//
//  SSPostContentCollectionViewCell.m
//  work
//
//  Created by ByteDance on 2022/7/4.
//

#import "PostContentCollectionViewCell.h"
@interface PostContentCollectionViewCell ()

@property (nonatomic, strong) VariableLabel *contentLabel;

@end

@implementation PostContentCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    //init contentLabel
    self.contentLabel = [[VariableLabel alloc]init];
    self.contentLabel.textColor = [UIColor blackColor];
    [self.contentLabel setFont:[UIFont systemFontOfSize:14]];
    //add view to self.view
    [self.contentView addSubview:self.contentLabel];
}

- (void)setupWithObject:(id)obj
{
    [self setupWithPost:obj];
}

- (void)setupWithPost:(Post *)post
{
    //设置数据 并调整宽高
    [self.contentLabel setupContent:post.content superWidth:self.frame.size.width fontSize:14.f];
}

@end
