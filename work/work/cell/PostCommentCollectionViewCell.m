//
//  PostCommentListCollectionViewCell.m
//  work
//
//  Created by ByteDance on 2022/7/6.
//

#import "PostCommentCollectionViewCell.h"
@interface PostCommentCollectionViewCell()

@property (nonatomic, strong) UIImageView *avatarImage;
@property (nonatomic, strong) UIImage *uiImage;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) UILabel *postTimeLabel;
@property (nonatomic ,strong) UIView *titleView;
@property (nonatomic, strong) InsideButton *diggButton;
@property (nonatomic, strong) InsideButton *delButton;
@property (nonatomic, strong)  LOTAnimationView * animation;
//@property(nonatomic)CGRect userImgRect,userLabelRect,userTimeRect,iconRect;
@property(nonatomic,strong)UILabel *diggCntLabel;
@property(nonatomic,strong) VariableLabel *titleLabel;


@end

@implementation PostCommentCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setupUI];
    }
    return self;
}

const int marginBottom = 18;
+ (double)viewHeight
{
    const int marginLeft = 16;
    CGRect userImgRect = CGRectMake(0, 0, 32, 32);
    CGRect userLabelRect = CGRectMake(userImgRect.size.width+userImgRect.origin.x+marginLeft, 0, 100, 22);
    CGRect userTimeRect = CGRectMake(userImgRect.size.width+marginLeft, userLabelRect.size.height, 100, 17);
    CGRect titleRect = CGRectMake(64, userImgRect.size.height+11.5, 291, 67);
    CGRect buttonRect = CGRectMake(0, 0, 14, 14);
    double buttonMarginTop = 11.5;
    double buttonMarginBottom = 17.5;
    
    return userLabelRect.size.height + userTimeRect.size.height + buttonRect.size.height + buttonMarginTop + buttonMarginBottom + marginBottom;
}

- (void)setupUI
{
    const double marginLeft = 12;
    CGRect userImgRect = CGRectMake(0, 0, 32, 32);
    CGRect userLabelRect = CGRectMake(userImgRect.size.width+userImgRect.origin.x+marginLeft, 0, 100, 22);
    CGRect userTimeRect = CGRectMake(userImgRect.size.width+marginLeft, userLabelRect.size.height, 120, 17);
    CGRect titleRect = CGRectMake(userImgRect.size.width+marginLeft, userImgRect.size.height+11.5, self.frame.size.width-(userImgRect.size.width+marginLeft), 0);
 
    // init avatar
    [self setupAvatarImgWithFrame:userImgRect];
    // init nickname
    [self setupNickNameLabelWithFrame:userLabelRect];
    // init posttime
    [self setupPostTimeLabelWithFrame:userTimeRect];
    // init title
    [self setupTitleViewWithFrame:titleRect];
    
    //add view to self.contentView
    [self.contentView addSubview:self.titleView];
    [self.contentView addSubview:self.userNameLabel];
    [self.contentView addSubview:self.avatarImage];
    [self.contentView addSubview:self.postTimeLabel];
    
    
    //titleView布局约束
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make){
        make.bottom.equalTo(self.mas_bottom);
        make.top.mas_equalTo(titleRect.origin.y);
        make.left.equalTo(self).with.offset(titleRect.origin.x);
        make.right.equalTo(self);
    }];
    
}


- (void)setupTitleViewWithFrame:(CGRect)frame
{
    self.titleView = [[UIView alloc]initWithFrame:frame];
    
    self.titleLabel = [[VariableLabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 24)];
    self.titleLabel.textColor = [UIColor blackColor];
    [self.titleLabel setFont:[UIFont systemFontOfSize:14]];
    
    CGRect buttonRect = CGRectMake(0, 0, 14, 14);
    
    self.diggButton = [[InsideButton alloc] initWithFrame:buttonRect];
    [self.diggButton setImage:[UIImage imageNamed:@"digg"] forState:UIControlStateNormal];
    [self.diggButton addTarget:self action:@selector(digButtonAction) forControlEvents:UIControlEventTouchDown];
    
    
    self.delButton = [[InsideButton alloc] init];
    [self.delButton setImage:[UIImage imageNamed:@"del"] forState:UIControlStateNormal];
    [self.delButton addTarget:self action:@selector(delCommentButton) forControlEvents:UIControlEventTouchUpInside];
    
    self.diggCntLabel = [self setupDiggCnt];
    self.animation = [LOTAnimationView animationNamed:@"ugc_digg_anim"];
    
    //add view
    [self.titleView addSubview:self.delButton];
    [self.titleView addSubview:self.diggButton];
    [self.titleView addSubview:self.titleLabel];
    [self.titleView addSubview:self.diggCntLabel];
   
    [self setupTitleViewAutolayoutWithFrame:frame];
   
}

- (UILabel *)setupDiggCnt
{
    UILabel *diggCntLabel = [UILabel new];
    diggCntLabel.font = [UIFont systemFontOfSize:12];
    diggCntLabel.textColor = [UIColor colorWithRed:24.0/255.0 green:24.0/255.0 blue:24.0/255.0 alpha:0.5];
    return diggCntLabel;
}

- (void)setupTitleViewAutolayoutWithFrame:(CGRect)frame
{
    CGRect delButtonRect = CGRectMake(frame.size.width-8, 0, 8, 8);
    double buttonMarginTop = 11.5;
    //布局约束
    [self.diggButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).with.offset(buttonMarginTop);
        make.width.mas_equalTo(11.5);
        make.height.mas_equalTo(11.5);
    }];
    [self.delButton mas_makeConstraints:^(MASConstraintMaker *make){
        make.centerY.equalTo(self.diggButton.mas_centerY);
//        make.top.equalTo(self.titleLabel.mas_bottom).with.offset(delButtonMarginTop);
        make.size.mas_equalTo(CGSizeMake(8, 8));
        make.left.with.offset(delButtonRect.origin.x);
    }];
    [self.diggCntLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.diggButton.mas_right).offset(5.25);
        make.centerY.equalTo(self.diggButton);
    }];
}
- (void)delCommentButton
{
    [self.viewcontroller delCommentButtonAction:self.index];
}

- (void)digButtonAction
{

    [self.diggButton setHidden:YES];
    [self.animation playWithCompletion:^(BOOL animationFinished) {
      // Do Something
    }];
    [self.titleView addSubview:self.animation];
    
    [self.animation mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.diggButton);
        make.centerY.equalTo(self.diggButton);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
    }];
    [self updateCommentCnt];
}

- (void)updateCommentCnt
{
    int cnt = [self.diggCntLabel.text intValue];
    cnt++;
    self.diggCntLabel.text = [NSString stringWithFormat:@"%d",cnt];
}
- (void)setupPostTimeLabelWithFrame:(CGRect)frame
{
    //时间
    self.postTimeLabel = [[UILabel alloc]initWithFrame:frame];
    self.postTimeLabel.textColor = [UIColor blackColor];
    self.postTimeLabel.backgroundColor = [UIColor clearColor];
    self.postTimeLabel.textAlignment = NSTextAlignmentLeft;
    UIFont *font = [UIFont systemFontOfSize:12];
    self.postTimeLabel.alpha = 0.5;
    [self.postTimeLabel setFont:font];
}

- (void)setupAvatarImgWithFrame:(CGRect)frame
{
    self.avatarImage = [[UIImageView alloc] init];
    [self.avatarImage setFrame:frame];
    self.avatarImage.layer.cornerRadius = CGRectGetWidth(self.avatarImage.bounds)/2.f;
    self.avatarImage.clipsToBounds = YES;
}
- (void)setupNickNameLabelWithFrame:(CGRect)frame
{
    self.userNameLabel = [[UILabel alloc]initWithFrame:frame];
    self.userNameLabel.textColor = [UIColor colorWithRed:24.0/255.f green:24/255.f blue:24/255.f alpha:0.7];
    self.userNameLabel.backgroundColor = [UIColor clearColor];
    self.userNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.userNameLabel setFont:[UIFont systemFontOfSize:14]];
}

- (void)setupWithObject:(id)obj
{
    [self setupWithComment:obj];
}

- (void)setupWithComment:(Comment *)comment
{
    [self.avatarImage sd_setImageWithURL:[NSURL URLWithString:comment.userInfo.avatar] placeholderImage:[UIImage imageNamed:@"pubImg"]];
//    self.avatarImage.image = [UIImage imageNamed:comment.userInfo.avatar];
    self.userNameLabel.text = comment.userInfo.userName;
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    NSString *date = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:comment.createTime]];
    self.postTimeLabel.text = date;
    
    [self.titleLabel setupContent:comment.commentContent superWidth:self.titleView.frame.size.width fontSize:14.f];
    
    self.diggCntLabel.text = [NSString stringWithFormat:@"%d",(int)comment.diggCount];
}



@end
