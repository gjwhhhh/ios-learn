//
//  MyUICollectionViewCell.m
//  MyFirstApp-01
//
//  Created by ByteDance on 2022/7/4.
//

#import "PostBookCollectionViewCell.h"
@interface PostBookCollectionViewCell()

@property(nonatomic,strong) UIView *bookView;
@property(nonatomic,strong) UILabel *bookTitleLabel, *authorLabel;
@property(nonatomic,strong) UIImageView *imageView;
@end

@implementation PostBookCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setupUI];
    }
    return self;
}

- (void)setupWithObject:(id)obj
{
    [self setupWithBook:obj];
}

- (void)setupWithBook:(Book *)book
{
    self.bookTitleLabel.text = book.bookName;
    self.authorLabel.text = book.author;
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:book.thumbUrl]
                      placeholderImage:[UIImage imageNamed:@"book"]];
}
+ (double)viewCellHeight
{
    CGRect imgRect = CGRectMake(0, 0, 60, 60);
    return imgRect.size.height+ 24;
}

- (void)setupUI
{
    self.backgroundColor = [UIColor colorWithRed:24.0/255 green:24.0/255 blue:24.0/255 alpha:0.03];
    self.layer.cornerRadius = 6;
    
    CGRect imgRect = CGRectMake(0, 0, 60, 60);
    CGRect bookTitleRect = CGRectMake(imgRect.size.width+12, 5.5, 168, 20);
    CGRect authorRect = CGRectMake(imgRect.size.width+12, bookTitleRect.size.height+4, bookTitleRect.size.width, 17);

    
    self.bookView = [[UIView alloc] initWithFrame:CGRectMake(12, 12, self.frame.size.width-24, self.frame.size.height-24)];
    self.imageView = [[UIImageView alloc]initWithFrame:imgRect];
    self.imageView.layer.cornerRadius = 6;
    self.imageView.clipsToBounds = YES;
    
    self.bookTitleLabel = [[UILabel alloc]initWithFrame:bookTitleRect];
    [self.bookTitleLabel setFont:[UIFont systemFontOfSize:12]];
    
    self.authorLabel = [[UILabel alloc]initWithFrame:authorRect];
    [self.authorLabel setFont:[UIFont systemFontOfSize:10]];
    self.authorLabel.textColor = [UIColor colorWithRed:24.0/255 green:24.0/255 blue:24.0/255 alpha:0.5];
    
    [self.bookView addSubview:self.bookTitleLabel];
    [self.bookView addSubview:self.authorLabel];
    [self.bookView addSubview:self.imageView];
    [self.contentView addSubview: self.bookView];
    
    UITapGestureRecognizer*tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
    [self addGestureRecognizer:tapGestureRecognizer];

}
- (void)click
{
//    NewViewController *cover = [[NewViewController alloc] init];
//    [self.viewController.navigationController pushViewController:cover animated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"点击了书籍" message:@"" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    //显示alertView
    [alert show];
}
@end
