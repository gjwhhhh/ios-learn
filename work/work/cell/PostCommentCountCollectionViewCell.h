//
//  PostCommentCountCollectionViewCell.h
//  work
//
//  Created by ByteDance on 2022/7/6.
//

#import "PostBasicCollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface PostCommentCountCollectionViewCell : PostBasicCollectionViewCell

+ (double)viewHeight;
@end

NS_ASSUME_NONNULL_END
