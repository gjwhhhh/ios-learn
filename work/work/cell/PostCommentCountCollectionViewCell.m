//
//  PostCommentCountCollectionViewCell.m
//  work
//
//  Created by ByteDance on 2022/7/6.
//

#import "PostCommentCountCollectionViewCell.h"


@interface PostCommentCountCollectionViewCell()
@property(nonatomic,strong) UILabel *CommentLabel;
@end

@implementation PostCommentCountCollectionViewCell

const int LabelHeight = 22;

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setupUI];
    }
    return self;
}
+ (double)viewHeight
{
    return LabelHeight;
}
- (void)setupUI
{
    self.CommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, LabelHeight)];
    self.CommentLabel.font = [UIFont systemFontOfSize:16];
    self.tintColor = [UIColor blackColor];
    [self.contentView addSubview:self.CommentLabel];
}
- (void)setupWithObject:(id)obj
{
    [self setupWithCommentCount:obj];
}
- (void)setupWithCommentCount:(NSNumber *)commentCnt
{
    int cnt = commentCnt.intValue;
    self.CommentLabel.text = [[NSString alloc]initWithFormat:@"%d条评论",cnt];
}

@end
