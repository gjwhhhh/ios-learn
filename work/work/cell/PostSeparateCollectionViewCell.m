//
//  PostSeparateCollectionViewCell.m
//  work
//
//  Created by ByteDance on 2022/7/8.
//

#import "PostSeparateCollectionViewCell.h"

@interface PostSeparateCollectionViewCell()
@property(nonatomic,strong) UIView * separateView;

@end

@implementation PostSeparateCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.backgroundColor = [UIColor colorWithRed:24/255.f green:24/255.f blue:24/255.f alpha:0.03];
}

+ (double)viewHeight
{
    return 10;
}
@end
