//
//  PostSeparateCollectionViewCell.h
//  work
//
//  Created by ByteDance on 2022/7/8.
//

#import <UIKit/UIKit.h>
#import "PostBasicCollectionViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface PostSeparateCollectionViewCell : PostBasicCollectionViewCell

+ (double)viewHeight;

@end

NS_ASSUME_NONNULL_END
