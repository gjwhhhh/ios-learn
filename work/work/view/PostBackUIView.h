//
//  PostBackUIView.h
//  work
//
//  Created by ByteDance on 2022/7/7.
//

#import <UIKit/UIKit.h>
#import "Lottie.h"
#import "Masonry.h"
#import "InsideButton.h"
NS_ASSUME_NONNULL_BEGIN

@interface PostBackUIView : UIView
@property (nonatomic ,strong) UIView *titleView;
@property (nonatomic, strong) UIButton *inputButton;
@property (nonatomic, strong) InsideButton*digBtn;
@property (nonatomic, strong) UILabel *cntLabel;
@end

NS_ASSUME_NONNULL_END
