//
//  CommentInputUIView.m
//  work
//
//  Created by ByteDance on 2022/7/11.
//

#import "CommentInputUIView.h"


#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define kTextFieldHeight 60

@interface CommentInputUIView()
@property(nonatomic,strong) UIButton *pubButton;
@property(nonatomic,strong) UIView *accessoryView;
@property(nonatomic,weak) ViewController *viewController;

@end


@implementation CommentInputUIView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame viewController:(ViewController *)viewController
{
    self = [super initWithFrame:frame];
    [self setupUI];
    
    self.viewController = viewController;
    return self;
}

- (void)setupUI
{
    [self setupSelfUI];
    [self setupTextViewUI];
    
    // add view
    [self addSubview:self.inputTextView];
    
    [self.inputTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(self.mas_right).offset(-20);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-24);
    }];
}
- (void) setupTextViewUI
{
    self.inputTextView = [[AutoResizeTextView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 30)];
    [self.inputTextView setBackgroundColor:[UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1]];
    
    [self setupAccessoryView];
    
    //add view to self.inputView
    self.inputTextView.inputAccessoryView = self.accessoryView;
    self.inputTextView.font = [UIFont systemFontOfSize:14 ];
}
- (void)setupAccessoryView
{
    CGRect accRect = CGRectMake(0, 0, 0, 48);
    self.accessoryView = [[UIView alloc]initWithFrame:accRect];
    //colorWithRed:24.0/255 green:24.0/255 blue:24.0/255 alpha:0.3
    [self.accessoryView setBackgroundColor:[UIColor whiteColor]];
    
    [self setupButton];
    
    [self.accessoryView addSubview:self.pubButton];
}

- (void)setupButton
{
    CGRect main = [[UIScreen mainScreen]bounds];
    self.pubButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [self.pubButton setFrame:CGRectMake(main.size.width*(287.0/375), (self.accessoryView.frame.size.height-28)/2.0, 68, 28)];
    [self.pubButton setTitle:@"发表" forState: UIControlStateNormal];
    self.pubButton.backgroundColor = [UIColor colorWithRed:1 green:95.0/255.0 blue:0 alpha:1];
    [self.pubButton setTintColor:[UIColor whiteColor]];
    self.pubButton.layer.cornerRadius = 14;
    self.pubButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.pubButton addTarget:self action:@selector(pubBtnAction) forControlEvents:UIControlEventTouchUpInside];
}

- (void)pubBtnAction
{
//    NSLog(@"%@",self.text);
    [self.viewController addPostCommentCell:self.inputTextView.text];
    self.inputTextView.text = @"";
}
- (void)setupSelfUI
{
    //background: rgba(248, 248, 248, 1);
    self.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1];
    self.layer.borderColor = UIColor.grayColor.CGColor;

    //左上和右上的圆角
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(12,12)];
    //创建 layer
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    //赋值
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
//    self.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
//    self.leftViewMode = UITextFieldViewModeAlways;
//    self.placeholder = @"发表评论";
    
    [self setupKeyBoardNotification];
}

- (void)setupKeyBoardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardNotification:) name:UIKeyboardWillHideNotification object:nil];
    
    self.inputTextView.delegate = self;
}

-(void)handleKeyboardNotification:(NSNotification *)notification{
    NSDictionary *userInfo = notification.userInfo;
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];

    //    CGRect keyboardScreenBeginFrame = [userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue] ;
    CGRect keyboardScreenEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];

    [UIView animateWithDuration:animationDuration animations:^{
        CGRect frame = self.frame;
        frame.origin.y = keyboardScreenEndFrame.origin.y - kTextFieldHeight;
        self.frame = frame;
    } completion:^(BOOL finished) {
        ;
    }];
    NSLog(@"notification");
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"dealloc");
}
@end
