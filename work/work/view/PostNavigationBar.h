//
//  PostNavigationBar.h
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PostNavigationBar : UINavigationBar
- (instancetype)initWithFrame:(CGRect)frame view:(UIViewController *)viewController;
@property(nonatomic,strong)UIBarButtonItem *backButtonItem,*detailButtonItem;
@end

NS_ASSUME_NONNULL_END
