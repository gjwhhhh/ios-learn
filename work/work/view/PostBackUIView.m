//
//  PostBackUIView.m
//  work
//
//  Created by ByteDance on 2022/7/7.
//

#import "PostBackUIView.h"
@interface PostBackUIView()

@property(nonatomic,strong) LOTAnimationView *animation;
@end

@implementation PostBackUIView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setupUI];
    }
    return self;
}


- (void)setupUI
{
    const double bookLeftMargin = 20;
    CGRect inputRect = CGRectMake(bookLeftMargin+ 7, 4, 275, 36);
    self.inputButton = [self setupInputButton:inputRect];
    
    CGRect buttonRect = CGRectMake(inputRect.size.width+inputRect.origin.x+20, (self.frame.size.height-14)/2, 14, 14);
    self.digBtn = [self setupBackDiggBtnFrame:buttonRect];
    
    CGRect labelRect = CGRectMake(buttonRect.origin.x+buttonRect.size.width+5, (self.frame.size.height-17)/2, 30, 17);
    self.cntLabel = [self setupBackCntUILabelFrame:labelRect cnt:292];
    
    [self addSubview:self.cntLabel];
    [self addSubview:self.digBtn];
    [self addSubview:self.inputButton];
    
    
}


- (UIButton *)setupInputButton:(CGRect)frame
{
    UIButton *inputButton = [[UIButton alloc]initWithFrame:frame];
    [inputButton setTitle:@"发表评论..." forState:UIControlStateNormal];
    inputButton.layer.cornerRadius = 17;
    inputButton.backgroundColor = [UIColor colorWithRed:24.0/255.0 green:24.0/255.0 blue:24.0/255.0 alpha:0.03];
    inputButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    inputButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
//    inputButton.configuration
    inputButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [inputButton setTitleColor:[UIColor colorWithRed:24.0/255.0 green:24.0/255.0 blue:24.0/255.0 alpha:0.3]forState:UIControlStateNormal];
//    [inputButton addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    return inputButton;
}

- (UIButton *)setupBackDiggBtnFrame:(CGRect)frame
{
    UIButton *diggButton = [[InsideButton alloc] initWithFrame:frame];
    [diggButton setImage:[UIImage imageNamed:@"digg"] forState:UIControlStateNormal];
    [diggButton addTarget:self action:@selector(diggBtnAction) forControlEvents:UIControlEventTouchDown];
    return diggButton;
}
- (void)diggBtnAction
{
    self.animation = [LOTAnimationView animationNamed:@"ugc_digg_anim"];
    
    [self.digBtn setHidden:YES];
    [self.animation playWithCompletion:^(BOOL animationFinished) {
      // Do Something
    }];
    [self addSubview:self.animation];
    
    [self.animation mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.digBtn);
        make.centerX.equalTo(self.digBtn);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
    }];
    [self updateCommentCnt];
}

- (void)updateCommentCnt
{
    int cnt = [self.cntLabel.text intValue];
    cnt++;
    self.cntLabel.text = [NSString stringWithFormat:@"%d",cnt];
}

- (UILabel *)setupBackCntUILabelFrame:(CGRect)frame cnt:(int)cnt
{
    UILabel *cntLabel = [[UILabel alloc]initWithFrame:frame];
    [cntLabel setFont:[UIFont systemFontOfSize:12]];
    [cntLabel setTintColor:[UIColor colorWithRed:24.0/255.0 green:24.0/255.0 blue:24.0/255.0 alpha:0.3]];
    cntLabel.text = [NSString stringWithFormat:@"%d",cnt];
    return cntLabel;
}

@end
