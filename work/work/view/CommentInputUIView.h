//
//  CommentInputUIView.h
//  work
//
//  Created by ByteDance on 2022/7/11.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "AutoResizeTextView.h"
NS_ASSUME_NONNULL_BEGIN

@interface CommentInputUIView : UIView<UITextViewDelegate>
@property(nonatomic,strong) AutoResizeTextView *inputTextView;
- (instancetype)initWithFrame:(CGRect)frame viewController:(ViewController *)viewController;
@end   

NS_ASSUME_NONNULL_END
