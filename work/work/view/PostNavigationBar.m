//
//  PostNavigationBar.m
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import "PostNavigationBar.h"
#import "ViewController.h"
@interface PostNavigationBar()
@property(nonatomic,weak)UIViewController *viewController;

@end

@implementation PostNavigationBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame view:(UIViewController *)viewController
{
    self = [super initWithFrame:frame];
    self.viewController = viewController;
    
    CGSize imgSize = CGSizeMake(20, 20);
    UIImage *backImage = [self scaleToSize:[UIImage imageNamed:@"back"] size:imgSize];
    self.backButtonItem  = [[UIBarButtonItem alloc] initWithImage:backImage style:UIBarButtonItemStyleDone target:self action:@selector(back:)];
    self.backButtonItem.tintColor = [UIColor blackColor];
    
    UIImage *detailImage = [self scaleToSize:[UIImage imageNamed:@"detail"] size:CGSizeMake(3, 16)];
    self.detailButtonItem  = [[UIBarButtonItem alloc] initWithImage:detailImage style:UIBarButtonItemStyleDone target:self action:@selector(detail:)];
    self.detailButtonItem.tintColor = [UIColor blackColor];

    
    UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"帖子详情"];
    navigationItem.leftBarButtonItem = self.backButtonItem;
    navigationItem.rightBarButtonItem = self.detailButtonItem;
    
    self.items = @[navigationItem];
    
    
    return self;
}

-(UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(size);
    // 绘制改变大小的图片
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}
- (void)back:(id)sender
{
}
- (void)detail:(id)sender
{
    UIAlertController *actionSheetController = [[UIAlertController alloc]init];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        NSLog(@"Tag 取消Button");
    }];
    UIAlertAction *reportAction = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        NSLog(@"Tag 举报Button");
    }];
 
    [actionSheetController addAction:reportAction];
    [actionSheetController addAction:cancelAction];
    
    actionSheetController.popoverPresentationController.sourceView = sender;
    
    
    [self.viewController presentViewController:actionSheetController animated:true completion:nil];
}


@end
