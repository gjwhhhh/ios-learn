//
//  PostDetailVO.h
//  work
//
//  Created by ByteDance on 2022/7/7.
//

#import <Foundation/Foundation.h>
#import "Post.h"
#import "Comment.h"
NS_ASSUME_NONNULL_BEGIN

@interface PostDetailVO : NSObject

@property(nonatomic, strong) Post *post;
@property(nonatomic, strong) NSMutableArray<Comment *> * comments;
@property(nonatomic) long commentCount;
@property(nonatomic) int nextOffset;

- (instancetype)initWithPost:(Post *)post Comments:(NSMutableArray<Comment *> *)comments commentCount:(int)commentCnt nextOffset:(int)nextOffset;

@end

NS_ASSUME_NONNULL_END
