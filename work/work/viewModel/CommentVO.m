//
//  CommentVO.m
//  work
//
//  Created by ByteDance on 2022/7/8.
//

#import "CommentVO.h"

@implementation CommentVO

- (instancetype)initWithNextOffset:(int)nextOffset comments:(NSMutableArray<Comment *> *)comments
{
    self = [super init];
    self.comments = comments;
    self.nextOffset = nextOffset;
    return self;
}

@end
