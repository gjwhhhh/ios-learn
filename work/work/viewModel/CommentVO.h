//
//  CommentVO.h
//  work
//
//  Created by ByteDance on 2022/7/8.
//

#import <Foundation/Foundation.h>
#import "Comment.h"
NS_ASSUME_NONNULL_BEGIN

@interface CommentVO : NSObject
@property(nonatomic)int nextOffset;
@property(nonatomic,strong) NSMutableArray<Comment *> *comments;

- (instancetype)initWithNextOffset:(int)nextOffset comments:(NSMutableArray<Comment *> *)comments;
@end

NS_ASSUME_NONNULL_END
