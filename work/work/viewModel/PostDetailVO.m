//
//  PostDetailVO.m
//  work
//
//  Created by ByteDance on 2022/7/7.
//

#import "PostDetailVO.h"

@implementation PostDetailVO

- (instancetype)initWithPost:(Post *)post Comments:(NSMutableArray<Comment *> *)comments commentCount:(int)commentCnt nextOffset:(int)nextOffset
{
    self = [super init];
    self.post = post;
    self.comments = comments;
    self.commentCount = commentCnt;
    self.nextOffset = nextOffset;
    return self;
}

@end
