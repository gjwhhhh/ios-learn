//
//  RequestJsonUtils.h
//  work
//
//  Created by ByteDance on 2022/7/7.
//

#import <Foundation/Foundation.h>
#import "PostDetailVO.h"
#import "Post.h"
#import "Book.h"
#import "Comment.h"
#import "CommentVO.h"
NS_ASSUME_NONNULL_BEGIN

@interface PostJsonToObjectUtils : NSObject

+ (PostDetailVO *)requestToPostDetailVOForNSDictionary:(NSDictionary *)postDictionary;

+ (CommentVO *)requestToCommentsForReqDict:(NSDictionary *)reqDict;
@end

NS_ASSUME_NONNULL_END
