//
//  RequestJsonUtils.m
//  work
//
//  Created by ByteDance on 2022/7/7.
//

#import "PostJsonToObjectUtils.h"

@implementation PostJsonToObjectUtils

+ (CommentVO *) requestToCommentsForReqDict:(NSDictionary *)reqDict
{
    NSMutableArray<Comment*>* comments = [self analyzeCommentWithNSDictionary:reqDict];
    int nextOffset = [[reqDict valueForKeyPath:@"data.next_offset"] intValue];
    CommentVO *commentVO = [[CommentVO alloc] initWithNextOffset:nextOffset comments:comments];
    return commentVO;
}


+ (PostDetailVO *)requestToPostDetailVOForNSDictionary:(NSDictionary *)postDictionary
{
    PostDetailVO *postDetailVO = [[PostDetailVO alloc]init];
    long cnt = [[postDictionary valueForKeyPath:@"data.comment_count"] longValue];
    postDetailVO.commentCount = cnt;
    postDetailVO.post = [self setupPostForNSDictionary:postDictionary];
    postDetailVO.comments = [self analyzeCommentWithNSDictionary:postDictionary];
    postDetailVO.nextOffset = [[postDictionary valueForKeyPath:@"data.next_offset"] intValue];
    return postDetailVO;
}
/*!
 * @brief 把格式化的JSON格式的字符串转换成字典
 * @param jsonString JSON格式的字符串
 * @return 返回字典
 */

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

+ (NSMutableArray<Comment *> *)analyzeCommentWithNSDictionary:(NSDictionary *)postDictionary
{
    NSMutableArray<Comment *> *commentsRes = [NSMutableArray array];
    NSArray<NSDictionary *> *commentsDict = [postDictionary valueForKeyPath:@"data.comment_list"];
    for(NSDictionary *commentDict in commentsDict){
        NSString *commentConent = [commentDict valueForKeyPath:@"comment_content.text"];
        NSString *commentId = [commentDict valueForKeyPath:@"comment_id"];
        double commentTime = [[commentDict valueForKeyPath:@"create_time"] doubleValue];
        long diggCount = [[commentDict valueForKeyPath:@"digg_count"] longValue];
        
        NSDictionary *userDict = [commentDict valueForKeyPath:@"comment_user_info"];
        UserInfo *userInfo = [self analyzeUserInfoForNSDictionary:userDict];
        
        Comment *commentItem = [[Comment alloc]initWithCommentId:commentId commentContent:commentConent createTime:commentTime userInfo:userInfo diggCount:diggCount];
        
        [commentsRes addObject:commentItem];
    }
    return commentsRes;
}

+ (NSString *)contentFilterCharacter:(NSString *)content
{
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"<p></p>"];

    NSString *trimmedString = [content stringByTrimmingCharactersInSet:set];
    NSString *str = [trimmedString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return str;
}
+ (Post *)setupPostForNSDictionary:(NSDictionary *)postDictionary
{
    //postId
    NSString *postId = [postDictionary valueForKeyPath:@"data.post.post_id"];
    //postContent
    NSString *contentJson = [postDictionary valueForKeyPath:@"data.post.content"];
    NSDictionary *contentDict = [PostJsonToObjectUtils dictionaryWithJsonString:contentJson];
    NSString *postContent = [self contentFilterCharacter:[contentDict valueForKeyPath:@"skeleton.data"]] ;
    
    
    //postCreateTime
    double postCreateTime = [[postDictionary valueForKeyPath:@"data.post.create_time"] doubleValue];
    //postTitle
    NSString *postTitle = [postDictionary valueForKeyPath:@"data.post.topic_info.title"];
    //userInfo
    NSDictionary *userInfoDict = [postDictionary valueForKeyPath:@"data.post.user_info"];
    UserInfo *userInfo = [self analyzeUserInfoForNSDictionary:userInfoDict];
    //books
    NSArray<Book *> *bookArray = [self setupBookForNSDictionary:postDictionary];
    Post *post = [[Post alloc]initWithpostId:postId content:postContent createTime:postCreateTime userInfo:userInfo title:postTitle books:bookArray];
    return post;
    
}
+ (UserInfo *)analyzeUserInfoForNSDictionary:(NSDictionary *)userInfoDict
{
    NSString *userAvatar = [userInfoDict valueForKeyPath:@"user_avatar"];
    NSString *userId = [userInfoDict valueForKeyPath:@"user_id"];
    NSString *userName = [userInfoDict valueForKeyPath:@"user_name"];
    
    UserInfo *userInfo = [[UserInfo alloc]initWithavatar:userAvatar userId:userId userName:userName];
    return userInfo;
}

+ (NSMutableArray<Book *> *)setupBookForNSDictionary:(NSDictionary *)postDictionary
{
    NSMutableArray<Book *> *books = [NSMutableArray array];
    NSArray<NSDictionary *> *bookDictArray = [postDictionary valueForKeyPath:@"data.post.book_info_list"];
    for(NSDictionary *book in bookDictArray){
        NSString *bookId = [book valueForKeyPath:@"book_id"];
        NSString *bookName = [book valueForKeyPath:@"book_name"];
        NSString *bookthumbUrl = [book valueForKeyPath:@"thumb_url"];
        NSString *bookAuthor = [book valueForKeyPath:@"author"];
        Book *bookItem = [[Book alloc] initWithbookId:bookId bookName:bookName thumbUrl:bookthumbUrl author:bookAuthor];
        [books addObject:bookItem];
    }
    return books;
}
@end
