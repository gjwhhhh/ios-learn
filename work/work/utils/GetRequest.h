//
//  getRequest.h
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "PostDetailVO.h"
NS_ASSUME_NONNULL_BEGIN

@interface GetRequest : NSObject

+ (NSDictionary *)getPostRequestOffset:(int)offset;
@end

NS_ASSUME_NONNULL_END
