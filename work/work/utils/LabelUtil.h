//
//  LabelUtil.h
//  work
//
//  Created by ByteDance on 2022/7/6.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface LabelUtil : NSObject

+ (double)HeightContent:(NSString *)content superWidth:(double)superWidth  fontSize:(CGFloat)fontSize;

@end

NS_ASSUME_NONNULL_END
