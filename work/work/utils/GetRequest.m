//
//  getRequest.m
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import "GetRequest.h"

@implementation GetRequest

static NSString * const URL =@"https://api5-lq.novelfm.com/novelfm/ugcapi/comments/get/group/7106764231739261447/v1/?sort_type=3&limit=20&group_type=2&topic_id=7106711250146640391&comment_id=&iid=1183539148097277&device_id=4195369462021304&ac=wifi&channel=local_test&aid=3040&app_name=novel_fm&version_code=400&version_name=4.0.0.17&device_platform=android&os=android&ssmix=a&device_type=Redmi%20K20&device_brand=Xiaomi&language=zh&os_api=30&os_version=11&manifest_version_code=400&resolution=1080*2296&dpi=440&update_version_code=40017&_rticket=1657275004045&need_personal_recommend=1&comment_tag_c=5&vip_state=0&category_style=1&ab_sdk_version=4297121&rom_version=miui_V125_V12.5.2.0.RFJCNXM&cdid=b9cfd33f-c1ba-42b3-a474-2b38bb386582";

static NSString *const URL2 = @"https://api5-lq.novelfm.com/novelfm/ugcapi/comments/get/group/7107510663953547789/v1/?sort_type=3&limit=20&group_type=2&topic_id=7107506879319769636&comment_id=&iid=1183539148097277&device_id=4195369462021304&ac=wifi&channel=local_test&aid=3040&app_name=novel_fm&version_code=400&version_name=4.0.0.17&device_platform=android&os=android&ssmix=a&device_type=Redmi%20K20&device_brand=Xiaomi&language=zh&os_api=30&os_version=11&manifest_version_code=400&resolution=1080*2296&dpi=440&update_version_code=40017&_rticket=1657275007210&need_personal_recommend=1&comment_tag_c=5&vip_state=0&category_style=1&ab_sdk_version=4297121&rom_version=miui_V125_V12.5.2.0.RFJCNXM&cdid=b9cfd33f-c1ba-42b3-a474-2b38bb386582";

+ (NSDictionary *)getPostRequestOffset:(int)offset
{
    //
    //1.创建会话管理者
      AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
      //2.发送GET请求
      /*
       第一个参数:请求路径(不包含参数).NSString
       第二个参数:字典(发送给服务器的数据~参数)
       第三个参数:progress 进度回调
       第四个参数:success 成功回调
                  task:请求任务
                  responseObject:响应体信息(JSON--->OC对象)
       第五个参数:failure 失败回调
                  error:错误信息
       响应头:task.response
       */
    NSDictionary *paramDict = @{@"offset":[NSString stringWithFormat:@"%d",offset]};
    
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    __block NSDictionary *res;
    [manager GET:URL2 parameters:paramDict headers:nil progress:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSLog(@"%@---%@",[responseObject class],responseObject);
        res = responseObject;
        dispatch_semaphore_signal(semaphore);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"请求失败--%@",error);
        dispatch_semaphore_signal(semaphore);
    }];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    return res;
}


@end
