//
//  LabelUtil.m
//  work
//
//  Created by ByteDance on 2022/7/6.
//

#import "LabelUtil.h"

@implementation LabelUtil

+ (double)HeightContent:(NSString *)content superWidth:(double)superWidth  fontSize:(CGFloat)fontSize
{
    //设置行间距
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:6.0f];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:content];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [content length])];
    // 设置字体大小
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize] range:NSMakeRange(0, [attributedString length])];
    //计算高度
    CGSize size = CGSizeMake(superWidth, MAXFLOAT);
    CGRect rect = [attributedString boundingRectWithSize:size options:NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin context:nil];
    
//    CGRect rect = [content boundingRectWithSize:size options:NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]} context:nil];
    
    return rect.size.height;
}
@end
