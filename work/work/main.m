//
//  main.m
//  work
//
//  Created by ByteDance on 2022/7/2.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "GetRequest.h"
#import "PostJsonToObjectUtils.h"
int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
        
        NSArray *originalStrings = @[@"Sauer",@"rea",@"BigNerd",@"Mississ"];
        NSMutableArray *devowlizedStrings = [NSMutableArray array];
        
        NSArray *vowels = [NSArray arrayWithObject:@[@"a",@"e",@"i",@"o",@"u"]];
        
        void (^devowelizer)(id, NSUInteger, BOOL *);
        devowelizer = ^(id string, NSUInteger i, BOOL *stop){
            NSMutableString *newString  = [NSMutableString stringWithString:string];
            for(NSString *s in vowels){
                NSRange fullRange = NSMakeRange(0, [newString length]);
                [newString replaceOccurrencesOfString:s withString:@"" options:NSCaseInsensitiveSearch range:fullRange];
            }
            [devowlizedStrings addObject:newString];
        };
        
        NSLog(@"%@",devowlizedStrings);
    }
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}


