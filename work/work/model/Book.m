//
//  Book.m
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import "Book.h"

@implementation Book
- (instancetype)initWithbookId:(NSString *)bookId bookName:(NSString *)bookName thumbUrl:(NSString *)thumbUrl author:(NSString *)author
{
    self.bookId = bookId;
    self.bookName = bookName;
    self.thumbUrl = thumbUrl;
    self.author = author;
    return self;
}
@end
