//
//  Comment.m
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import "Comment.h"

@implementation Comment

- (instancetype)initWithCommentId:(NSString *)commentId commentContent:(NSString *)commentContent createTime:(double)createTime userInfo:(UserInfo *)userInfo diggCount:(long)diggCount
{
    self.commentContent = commentContent;
    self.commentId = commentId;
    self.createTime = createTime;
    self.userInfo = userInfo;
    self.diggCount = diggCount;
    return self;
}
@end
