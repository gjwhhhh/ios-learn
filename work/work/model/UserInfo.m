//
//  UserInfo.m
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import "UserInfo.h"

@implementation UserInfo

- (instancetype)initWithavatar:(NSString*)avatar userId:(NSString *)userId userName:(NSString *)userName
{
    self.avatar = avatar;
    self.userId = userId;
    self.userName = userName;
    return self;
}
@end
