//
//  UserInfo.h
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserInfo : NSObject
@property(nonatomic,strong)NSString *avatar;
@property(nonatomic,strong)NSString *userId;
@property(nonatomic,strong)NSString *userName;

- (instancetype)initWithavatar:(NSString*)avatar userId:(NSString *)userId userName:(NSString *)userName;
@end

NS_ASSUME_NONNULL_END
