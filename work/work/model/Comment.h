//
//  Comment.h
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import <Foundation/Foundation.h>
#import "UserInfo.h"
NS_ASSUME_NONNULL_BEGIN

@interface Comment : NSObject
@property(nonatomic,strong)NSString *commentId;
@property(nonatomic,strong)NSString *commentContent;
@property(nonatomic)double createTime;
@property(nonatomic,strong)UserInfo *userInfo;
@property(nonatomic)long diggCount;

- (instancetype)initWithCommentId:(NSString *)commentId commentContent:(NSString *)commentContent createTime:(double)createTime userInfo:(UserInfo *)userInfo diggCount:(long)diggCount;

@end

NS_ASSUME_NONNULL_END
