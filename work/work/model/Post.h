//
//  Post.h
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import <Foundation/Foundation.h>
#import "UserInfo.h"
#import "Book.h"
#import <IGListDiffKit/IGListDiffable.h>

NS_ASSUME_NONNULL_BEGIN

@interface Post : NSObject <IGListDiffable>
@property(nonatomic,strong) NSString *postId;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *content;
@property(nonatomic) double createTime;
@property(nonatomic,strong) UserInfo *userInfo;
@property(nonatomic,strong) NSArray *books;

- (instancetype)initWithpostId:(NSString *)postId content:(NSString *)content createTime:(double)createTime userInfo:(UserInfo *)userInfo title:(NSString *)title books:(NSArray *)books;
@end

NS_ASSUME_NONNULL_END
