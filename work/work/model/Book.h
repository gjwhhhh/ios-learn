//
//  Book.h
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Book : NSObject
@property(nonatomic)NSString * bookId;
@property(nonatomic,strong)NSString *bookName;
@property(nonatomic,strong)NSString *thumbUrl;
@property(nonatomic,strong)NSString *author;

- (instancetype)initWithbookId:(NSString *)bookId bookName:(NSString *)bookName thumbUrl:(NSString *)thumbUrl author:(NSString *)author;

@end

NS_ASSUME_NONNULL_END
