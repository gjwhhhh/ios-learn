//
//  Post.m
//  work
//
//  Created by ByteDance on 2022/7/5.
//

#import "Post.h"

@implementation Post
- (instancetype)initWithpostId:(NSString *)postId content:(NSString *)content createTime:(double)createTime userInfo:(UserInfo *)userInfo title:(NSString *)title books:(NSArray *)books
{
    self.postId = postId;
    self.content = content;
    self.createTime = createTime;
    self.userInfo = userInfo;
    self.title = title;
    self.books = books;
    return self;
}

- (id<NSObject>)diffIdentifier
{
    return self;
}
- (BOOL)isEqualToDiffableObject:(id<IGListDiffable>)object
{
    return self == object;
}

- (id)copyWithZone:(NSZone *)zone
{
    Post *post = [[Post alloc] initWithpostId:self.postId content:self.content createTime:self.createTime userInfo:self.userInfo title:self.title books:self.books];
    return post;
}

@end
  
