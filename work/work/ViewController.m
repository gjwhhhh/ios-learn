//
//  ViewController.m
//  work
//
//  Created by ByteDance on 2022/7/2.
//

#import "ViewController.h"
#import "CommentInputUIView.h"
#import "PostCommentCollectionViewCell.h"
#import "PostBookCollectionViewCell.h"

#import "AutoResizeTextView.h"
#import "PostHeaderCollectionViewCell.h"
#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define kTextFieldHeight 78

@interface ViewController ()

@property(nonatomic) double contentHeight;
@property(nonatomic,strong) UICollectionView *collectionView;
@property(strong,nonatomic) PostBackUIView *backUIView;
@property(nonatomic,strong) NSMutableArray *cellViewData;
@property(strong,nonatomic) PostNavigationBar *navigationBar;
@property(strong,nonatomic) CommentInputUIView *xTextField;

@property(nonatomic) int nextOffset;
@property(nonatomic) long commentCnt;
@property(strong,nonatomic) NSMutableArray<Comment *> *comments;

@property(strong,nonatomic) Post *post;
@property(strong,nonatomic) NSArray *books;

@property(nonatomic) Boolean isScrollToEnd;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    //init 导航栏
    [self setUpNavigationBar];
    //init data
    [self setupData];
    //init UIcollectionView
    [self setUpBookCollectionView];
    //init comment textField
    [self setupCommentTextField];
    //init back
    [self setupBack];
    
    //add view to self.view
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.xTextField];
    [self.view addSubview:self.backUIView];
    
}

-(void)delCommentButtonAction:(int)delIndex {
    [self.comments removeObjectAtIndex:delIndex];
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)setupData
{
    self.nextOffset = 0;
    PostDetailVO *postDetailVO = [PostJsonToObjectUtils requestToPostDetailVOForNSDictionary:[GetRequest getPostRequestOffset:self.nextOffset]];
    self.post = postDetailVO.post;
    self.books = postDetailVO.post.books;
    self.comments = postDetailVO.comments;
    self.commentCnt = postDetailVO.commentCount;
    self.nextOffset =  postDetailVO.nextOffset;
}


# pragma mark ViewCreate

- (void)setupCommentTextField
{
    self.xTextField = [[CommentInputUIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT - kTextFieldHeight, SCREEN_WIDTH, kTextFieldHeight) viewController:self];
    self.xTextField.inputTextView.delegate = self;
    [self.xTextField setHidden:YES];
    
    //添加点击背景视图 收起键盘的手势
    UITapGestureRecognizer*tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide)];

    [self.view addGestureRecognizer:tapGestureRecognizer];
}
- (void)keyboardHide
{
    [self.xTextField.inputTextView resignFirstResponder];
    [self.xTextField setHidden:YES];
    [self.backUIView setHidden:NO];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self keyboardHide];
    return YES;
}

//初始化导航栏
- (void)setUpNavigationBar
{
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat navigationBarHeight = 44;
//    self.view.safeAreaInsets.top;
    self.navigationBar = [[PostNavigationBar alloc] initWithFrame:CGRectMake(0, 30, screen.size.width, navigationBarHeight) view:self];
//    [self.view addSubview:self.navigationBar];
    self.navigationItem.leftBarButtonItem = self.navigationBar.backButtonItem;
    self.navigationItem.rightBarButtonItem = self.navigationBar.detailButtonItem;
    self.navigationItem.title = @"帖子详情";
}

- (void)setupBack
{
    CGRect backRect = CGRectMake(0, SCREEN_HEIGHT-kTextFieldHeight, SCREEN_WIDTH, 44);
    self.backUIView = [[PostBackUIView alloc]initWithFrame:backRect];
//    添加点击评论按钮的点击事件，将键盘弹出
    [self.backUIView.inputButton addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
}
-(void)backBtnAction
{
    [self.backUIView setHidden:YES];
    
    [self.xTextField setHidden:NO];
    [self.xTextField.inputTextView becomeFirstResponder];
}

#pragma mark Action
- (void)diggBtnAction
{
    NSLog(@"like~!");
}
- (UIButton *)setupBackDiggBtnFrame:(CGRect)frame
{
    UIButton *diggButton = [[UIButton alloc] initWithFrame:frame];
    [diggButton setImage:[UIImage imageNamed:@"digg"] forState:UIControlStateNormal];
    [diggButton addTarget:self action:@selector(diggBtnAction) forControlEvents:UIControlEventTouchDown];
    return diggButton;
}
- (UILabel *)setupBackCntUILabelFrame:(CGRect)frame cnt:(int)cnt
{
    UILabel *cntLabel = [[UILabel alloc]initWithFrame:frame];
    [cntLabel setFont:[UIFont systemFontOfSize:12]];
    [cntLabel setTintColor:[UIColor colorWithRed:24.0/255.0 green:24.0/255.0 blue:24.0/255.0 alpha:0.3]];
    cntLabel.text = [NSString stringWithFormat:@"%d",cnt];
    return cntLabel;
}



#pragma mark UICellectionViewCell
- (void)addPostCommentCell:(NSString *)commentContent;
{
    double nowTime = [[NSDate date] timeIntervalSince1970];

    Comment *comment = [[Comment alloc] initWithCommentId:@"" commentContent:commentContent createTime:nowTime userInfo:self.post.userInfo diggCount:0];
    [self.comments insertObject:comment atIndex:0];
    [self.collectionView reloadData];
    [self keyboardHide];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(![scrollView isKindOfClass:[UICollectionView class]]){
        return ;
    }
    if(_isScrollToEnd){
        return ;
    }
    CGFloat height = scrollView.frame.size.height;
    CGFloat contentYoffset = scrollView.contentOffset.y;
    CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
    if(distanceFromBottom - 200 <= height){
        NSLog(@"滚动到底部了");
        if(self.commentCnt == self.nextOffset){
            _isScrollToEnd = YES;
        }
        else{
            NSLog(@"发起网络请求！");
            [self scrollEndSendNetWorkingAndReloadData];
            _isScrollToEnd = NO;
        }
    }
}
- (void)scrollEndSendNetWorkingAndReloadData
{
    CommentVO *commentVO = [PostJsonToObjectUtils requestToCommentsForReqDict:[GetRequest getPostRequestOffset:self.nextOffset] ];
    [self.comments addObjectsFromArray:commentVO.comments];
    self.nextOffset = commentVO.nextOffset;
    [self.collectionView reloadData];
}

const double bookLeftMargin = 20;
- (void)setUpBookCollectionView
{
    CGRect main = [[UIScreen mainScreen] bounds];
    
    CGRect collectionRect = CGRectMake(0, self.navigationBar.frame.origin.y+self.navigationBar.frame.size.height, main.size.width, main.size.height-self.navigationBar.frame.origin.y-self.navigationBar.frame.size.height-kTextFieldHeight);
    
    UICollectionViewFlowLayout *layout=[self setupCollectionFlowLayout];
    
    self.collectionView =[[UICollectionView alloc] initWithFrame:collectionRect collectionViewLayout:layout];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    [self.collectionView setTintColor:[UIColor whiteColor]];
    [self.collectionView  setDataSource:self];
    [self.collectionView  setDelegate:self];

    // 类模版的注册
    NSArray *classArray = @[[UICollectionViewCell class],[PostHeaderCollectionViewCell class],[PostContentCollectionViewCell class],[UICollectionViewCell class], [PostBookCollectionViewCell class], [PostCommentCountCollectionViewCell class],[PostSeparateCollectionViewCell class], [PostCommentCollectionViewCell class]];
    [self registerCollectionView:self.collectionView  ClassArray:classArray];
}

- (UICollectionViewFlowLayout *)setupCollectionFlowLayout
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //行内块间距
    layout.minimumInteritemSpacing = 0;
    //行间距
    layout.minimumLineSpacing = 16;
    layout.itemSize = CGSizeMake(self.view.bounds.size.width-40, 50);
    //Collection header 的大小
    layout.headerReferenceSize = CGSizeMake(0,0);
    //排列方向
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    return layout;
}

//UICollectionView注册类的函数
- (void)registerCollectionView:(UICollectionView *)collectionView ClassArray:(NSArray<Class> *)classArray
{
    for(Class regClass in classArray){
        [collectionView registerClass:regClass forCellWithReuseIdentifier:NSStringFromClass(regClass)];
    }
}

#pragma mark - UICollectionDataSource
//Cell的总数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4 + self.books.count + self.comments.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //判断是哪个自定义的CollectionViewCell类
    Class cellClass = [self collectionCellClassForIndex:(int)indexPath.item];
    
    __auto_type cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cellClass) forIndexPath:indexPath];
    //加载自定义的CollectionViewCell类的数据
    [self collectionCellSetupObjectForcell:cell index:(int)indexPath.item];
    return cell;

}

//每个Cell的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize res = [self collectionCellItemSizeForCollectionFrame:collectionView.frame Index:(int)indexPath.item];
    
    return res;
}
#pragma mark - private
- (void)collectionCellSetupObjectForcell:(id)cell index:(int)index
{
    if ([cell isKindOfClass:[PostHeaderCollectionViewCell class]] ) {
        __auto_type basicCell = (PostHeaderCollectionViewCell *)cell;
        basicCell.viewController = self;
        [basicCell setupWithObject:self.post];
    }else if( [cell isKindOfClass:[PostContentCollectionViewCell class]]){
        __auto_type basicCell = (PostHeaderCollectionViewCell *)cell;
        [basicCell setupWithObject:self.post];
    }
    else if([cell isKindOfClass:[PostBookCollectionViewCell class]]){
        __auto_type basicCell = (PostBookCollectionViewCell *)cell;
        basicCell.viewController = self;
        [basicCell setupWithObject:(Book *)self.books[index-2]];
    }else if([cell isKindOfClass:[PostSeparateCollectionViewCell class]]){
        
    }
    else if([cell isKindOfClass:[PostCommentCountCollectionViewCell class]]){
        __auto_type basicCell = (PostBasicCollectionViewCell *)cell;
        [basicCell setupWithObject:[[NSNumber alloc] initWithLong:self.commentCnt]];
    }else if([cell isKindOfClass:[PostCommentCollectionViewCell class]]){
        __auto_type basicCell = (PostCommentCollectionViewCell *)cell;
        basicCell.index = (int)(index-(4+self.books.count));
        basicCell.viewcontroller = self;
        [basicCell setupWithObject:self.comments[index-(4+self.books.count)]];
    }
}

- (Class)collectionCellClassForIndex:(int)index
{
    Class cellClass = [UICollectionViewCell class];
    if(index == 0){
        cellClass = [PostHeaderCollectionViewCell class];
    }
    else if(index == 1){
        cellClass = [PostContentCollectionViewCell class];
    }
    else if (index >= 2 && index-2 < self.books.count){
        cellClass = [PostBookCollectionViewCell class];
    }else if(index == 2+self.books.count){
        cellClass = [PostSeparateCollectionViewCell class];
    } else if(index == 3+self.books.count){
        cellClass = [PostCommentCountCollectionViewCell class];
    }else if(index >= 4+self.books.count && index-(4+self.books.count) < self.comments.count){
        cellClass = [PostCommentCollectionViewCell class];
    }
    return cellClass;
}

- (CGSize)collectionCellItemSizeForCollectionFrame:(CGRect)collectionFrame Index:(int)index
{
    CGSize res = CGSizeMake(CGRectGetWidth(collectionFrame)-bookLeftMargin*2, 84);
    if(index == 0){
        res.height = [PostHeaderCollectionViewCell viewCellHeight];
    }
    else if(index == 1){
        res.height = [LabelUtil HeightContent:self.post.content superWidth:res.width fontSize:14];
    }
    else if (index >= 2 && index-2 < self.books.count){
        res.height = [PostBookCollectionViewCell viewCellHeight];
    }else if(index == 2+self.books.count){
        res.height  = [PostSeparateCollectionViewCell viewHeight];
        res.width = CGRectGetWidth(collectionFrame);
    } else if(index == 3+self.books.count){
        res.height = [PostCommentCountCollectionViewCell viewHeight];
       
    }else if(index >= 4+self.books.count && index-(4+self.books.count) < self.comments.count){
        res.height = [PostCommentCollectionViewCell viewHeight] + [LabelUtil HeightContent:self.comments[index-(4+self.books.count)].commentContent superWidth:CGRectGetWidth(collectionFrame) fontSize:14];
    }
    return  res;
}

@end
