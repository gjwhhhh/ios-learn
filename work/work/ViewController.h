//
//  ViewController.h
//  work
//
//  Created by ByteDance on 2022/7/2.
//

#import <UIKit/UIKit.h>
#import "PostBackUIView.h"
#import "PostNavigationBar.h"

//models
#import "UserInfo.h"
#import "Post.h"
#import "PostDetailVO.h"
#import "CommentVO.h"
// cells

#import "PostContentCollectionViewCell.h"
#import "PostCommentCountCollectionViewCell.h"
#import "PostSeparateCollectionViewCell.h"
//utils
#import "LabelUtil.h"
#import "SDWebImage/SDWebImage.h"
//networking
#import "GetRequest.h"
#import "PostJsonToObjectUtils.h"

@interface ViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate>


-(void)delCommentButtonAction:(int)delIndex;
- (void)addPostCommentCell:(NSString *)commentContent;
@end

