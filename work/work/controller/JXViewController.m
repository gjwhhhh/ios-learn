//
//  JXViewController.m
//  work
//
//  Created by ByteDance on 2022/7/29.
//

#import "JXViewController.h"
#import "JXCategoryViewController.h"

@interface JXViewController ()

@end

@implementation JXViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    JXCategoryViewController *vc = [[JXCategoryViewController alloc] init];
    vc.title = @"test";
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
