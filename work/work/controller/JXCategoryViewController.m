//
//  JXCategoryViewController.m
//  work
//
//  Created by ByteDance on 2022/7/29.
//

#import "JXCategoryViewController.h"
#import "JXCategoryView.h"
#import "ListViewController.h"

@interface JXCategoryViewController ()<JXCategoryViewDelegate, JXCategoryListContainerViewDelegate>
@property (nonatomic, strong) JXCategoryTitleView *categoryView;
@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;
@property (nonatomic, strong) NSArray<NSString *> *titles;
@property (nonatomic, strong) NSMutableArray <ListViewController *> *lists;
@end

@implementation JXCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lists = [NSMutableArray array];
    self.navigationController.navigationBar.hidden = YES;
    
    [self setupTitleView];
    [self setupListContainerView];
}

- (void)setupTitleView
{
    CGRect windowsSize = self.view.frame;
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, windowsSize.size.width, 50)];
    self.categoryView.delegate = self;
    [self.view addSubview:self.categoryView];
    
    //set data
    self.categoryView.titles = @[@"螃蟹", @"麻辣小龙虾", @"苹果"];
    self.categoryView.titleColorGradientEnabled = YES;
    //add indicator
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorColor = [UIColor redColor];
    lineView.indicatorWidth = JXCategoryViewAutomaticDimension;
    self.categoryView.indicators = @[lineView];
    //layout
    self.categoryView.translatesAutoresizingMaskIntoConstraints = NO;
    CGFloat categoryViewHeight = 50;
    [self.categoryView.heightAnchor constraintEqualToConstant:categoryViewHeight].active = YES;
    [self.categoryView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.categoryView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.categoryView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
}

- (void)setupListContainerView
{
    CGFloat categoryViewHeight = 50;
    _listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
//    self.listContainerView.frame = CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-50);
    [self.view addSubview:self.listContainerView];
    self.listContainerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.categoryView.bottomAnchor constraintEqualToAnchor:self.listContainerView.topAnchor].active = YES;
    [self.listContainerView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.listContainerView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    CGFloat listContainerViewHeight = self.view.bounds.size.height - [UIApplication sharedApplication].keyWindow.safeAreaInsets.top - categoryViewHeight;
    [self.listContainerView.heightAnchor constraintEqualToConstant:listContainerViewHeight].active = YES;
    
    self.categoryView.listContainer =  self.listContainerView;
}

#pragma mark - JXCategoryListContentViewDelegate
// 返回列表的数量
- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}
// 根据下标 index 返回对应遵守并实现 `JXCategoryListContentViewDelegate` 协议的列表实例
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    ListViewController *list = [[ListViewController alloc] init];
    return list;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
