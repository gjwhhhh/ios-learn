//
//  ListViewController.h
//  work
//
//  Created by ByteDance on 2022/7/29.
//

#import <UIKit/UIKit.h>
#import "JXCategoryView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ListViewController : UIViewController <JXCategoryListContentViewDelegate>
@property (nonatomic, copy) void (^scrollViewDidScrollBlock)(UIScrollView *);
@property (nonatomic, copy) void (^willBeginDragging)(UIScrollView *);
@property (nonatomic, copy) void (^didEndDragging)(UIScrollView *);

- (void)stopScrolling;
@end

NS_ASSUME_NONNULL_END
