//
//  PersonSectionController.m
//  work
//
//  Created by ByteDance on 2022/8/3.
//

#import "PersonSectionController.h"
#import "PostHeaderCollectionViewCell.h"
#import "PostSeparateCollectionViewCell.h"
#import "PostContentCollectionViewCell.h"
#import "PostBookCollectionViewCell.h"
#import "Post.h"

@interface PersonSectionController()
@property(nonatomic, strong) Post *post;
@end

@implementation PersonSectionController

#pragma mark - IGListSectionController Overrides
- (NSInteger)numberOfItems
{
    return 3;
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
    const CGFloat width = self.collectionContext.containerSize.width;
    const CGFloat height = 88.0;
    return CGSizeMake(width, height);
}

- (__kindof UICollectionViewCell *)cellForItemAtIndex:(NSInteger)index
{
    Class cellClass;
    if(index == 0){
        cellClass = [PostHeaderCollectionViewCell class];
    } else if (index == 1) {
        cellClass = [PostContentCollectionViewCell class];
    } else {
        cellClass = [PostSeparateCollectionViewCell class];
    }
    id cell = [self.collectionContext dequeueReusableCellOfClass:cellClass forSectionController:self atIndex:index];
    if([cell isKindOfClass:[PostHeaderCollectionViewCell class]]){
        [(PostHeaderCollectionViewCell *)cell setupWithObject:self.post];
    } else if ([cell isKindOfClass:[PostContentCollectionViewCell class]]){
        [(PostContentCollectionViewCell *)cell setupWithObject:self.post];
    }
    return cell;
}

- (void)didUpdateToObject:(id)object
{
    _post = object;
}

#pragma mark - getter

@end
