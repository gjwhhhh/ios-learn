//
//  PersonViewController.m
//  work
//
//  Created by ByteDance on 2022/8/3.
//

#import "PersonViewController.h"
#import "PersonSectionController.h"
#import "Post.h"
#import "PostJsonToObjectUtils.h"
#import "PostDetailVO.h"
#import "GetRequest.h"

@interface PersonViewController() <IGListAdapterDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) IGListAdapter *adapter;
@property (nonatomic, copy) NSArray<Post *> *data;
@end

@implementation PersonViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    PostDetailVO *postDetailVO = [PostJsonToObjectUtils requestToPostDetailVOForNSDictionary:[GetRequest getPostRequestOffset:0]];
    Post* post1 = postDetailVO.post.copy;
    post1.title = @"啦啦啦阿拉啦啦";
    Post *post2 = postDetailVO.post.copy;
    
    self.data = @[
        post1,
        post2
    ];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:[UICollectionViewFlowLayout new]]
    ;
    [self.view addSubview:self.collectionView];
    self.adapter = [[IGListAdapter alloc] initWithUpdater:[[IGListAdapterUpdater alloc] init] viewController:self];

    self.adapter.collectionView = self.collectionView;
    self.adapter.dataSource = self;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.collectionView.frame = self.view.bounds;
}


#pragma mark - IGListAdapterDataSource
- (NSArray<id<IGListDiffable>> *)objectsForListAdapter:(IGListAdapter *)listAdapter {
    return self.data;
}

- (IGListSectionController *)listAdapter:(IGListAdapter *)listAdapter sectionControllerForObject:(id)object {
    
    return [PersonSectionController new];
}

- (UIView *)emptyViewForListAdapter:(IGListAdapter *)listAdapter {
    return nil;
}

@end
