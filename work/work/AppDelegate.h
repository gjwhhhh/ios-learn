//
//  AppDelegate.h
//  work
//
//  Created by ByteDance on 2022/7/2.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property(nonatomic, strong) UIWindow *window;

@end

