//
//  InsideButton.m
//  work
//
//  Created by ByteDance on 2022/7/12.
//

#import "InsideButton.h"

@implementation InsideButton


- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    CGRect bounds = self.bounds;
    //宽高希望扩展的范围，可根据具体需求设置
    CGFloat widthDelta =30;
    CGFloat heightDelta =30;
    
    //相当于bounds 上下左右都增加了10的额外
    //注意这里是负数，扩大了之前的bounds的范围
    bounds = CGRectInset(bounds, -0.5 * widthDelta, -0.5 * heightDelta);
    //点击的点是否在这个范围
    return CGRectContainsPoint(bounds, point);
}

@end
