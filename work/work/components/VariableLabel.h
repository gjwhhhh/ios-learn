//
//  VariableLabel.h
//  work
//
//  Created by ByteDance on 2022/7/6.
//

#import <UIKit/UIKit.h>
#import "LabelUtil.h"
#import "UILabel+String.h"
NS_ASSUME_NONNULL_BEGIN

@interface VariableLabel : UILabel

- (void)setupContent:(NSString *)content superWidth:(double)superWidth fontSize:(double)fontSize;
@end

NS_ASSUME_NONNULL_END
