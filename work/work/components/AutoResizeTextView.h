//
//  AutoResizeTextView.h
//  work
//
//  Created by ByteDance on 2022/7/11.
//

#import <UIKit/UIKit.h>

#import "testViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface AutoResizeTextView : UITextView<UITextViewDelegate>

@end

NS_ASSUME_NONNULL_END
