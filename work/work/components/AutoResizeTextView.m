//
//  AutoResizeTextView.m
//  work
//
//  Created by ByteDance on 2022/7/11.
//

#import "AutoResizeTextView.h"
#define MIN_HEIGHT 30
#define MAX_HEIGHT 60

@interface AutoResizeTextView()
@end

@implementation AutoResizeTextView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setContentSize:(CGSize)contentSize
{
    super.contentSize = contentSize;
    [self invalidateIntrinsicContentSize];
}

- (CGSize)intrinsicContentSize
{
    if(self.contentSize.height <= MIN_HEIGHT){
        return CGSizeMake(self.contentSize.width, MIN_HEIGHT);
    }
    if(self.contentSize.height > MAX_HEIGHT){
        return CGSizeMake(self.contentSize.width, MAX_HEIGHT);
    }
    return self.contentSize;
    
}


@end
