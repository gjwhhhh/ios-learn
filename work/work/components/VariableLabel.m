//
//  VariableLabel.m
//  work
//
//  Created by ByteDance on 2022/7/6.
//

#import "VariableLabel.h"

@implementation VariableLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.numberOfLines = 0;
    [self sizeToFit];
    
}
- (void)setupContent:(NSString *)content superWidth:(double)superWidth fontSize:(double)fontSize
{
    self.text = content;
    //设置行间距
    [self setText:content lineSpacing:6.0];
    
    //计算label高度
    double height = [LabelUtil HeightContent:content superWidth:superWidth fontSize:fontSize];
    
    CGRect labelFrame = self.frame;
    labelFrame.size = CGSizeMake(superWidth, height);
    self.frame = labelFrame;
}
@end
