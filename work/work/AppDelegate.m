//
//  AppDelegate.m
//  work
//
//  Created by ByteDance on 2022/7/2.
//

#import "AppDelegate.h"
#import "JXCategoryViewController.h"
#import "ListViewController.h"
#import "JXViewController.h"
#import "PersonViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.backgroundColor = UIColor.whiteColor;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:PersonViewController.new];
//    navigationController.navigationBar.hidden = YES;
    self.window.rootViewController = navigationController;

    [self.window makeKeyAndVisible];
    
    return YES;
}














/*
 
 - (UIView *)setupBack
 {
     CGRect rootRect = self.rootCollectionView.frame;
     UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(rootRect.origin.x, rootRect.size.height+rootRect.origin.y, rootRect.size.width, 36)];
 //    backView.backgroundColor = [UIColor redColor];
     
     CGRect inputRect = CGRectMake(rootRect.origin.x+7, 4, 275, 36);
 //    UIButton *inputButton = [[UIButton alloc]initWithFrame:buttonRect];
 //    [inputButton setTitle:@"发表评论" forState:UIControlStateNormal];
     UITextField *inputField = [[UITextField alloc]initWithFrame:inputRect];
     
     inputField.borderStyle = UITextBorderStyleRoundedRect;
     inputField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
     [inputField setFont:[UIFont boldSystemFontOfSize:12]];
     inputField.placeholder = @"发表评论...";
     
     [backView addSubview:inputField];
     return backView;
 }
 */


//静态数据
//- (void)setupStaticData
//{
//
//    self.books = @[
//        [[Book alloc]initWithbookId:@"2" bookName:@"神雕侠侣（第二卷）新修版" thumbUrl:@"book" author:@"金庸"],
//    [[Book alloc]initWithbookId:@"2" bookName:@"神雕侠侣（第二卷）新修版" thumbUrl:@"book" author:@"金庸"]
//    ] ;
//    NSString *content = @"第一本:构思新颖，很喜欢的一本书,第二本:角色独立，有特色，每位角色都有各色的性格，反派智商在线，主角心思细腻，就吐槽一点我都听了几千集了，咋还不当我是自己人，我为大哥战今生啊";
//    UserInfo *user1 = [[UserInfo alloc]initWithavatar:@"pubImg" userId:@"123123" userName:@"陈二狗"];
//    self.post = [[Post alloc] initWithpostId:@"1" content:content createTime:1654859114 userInfo:user1  title:@"写一个关于春天浪漫的故事" books:self.books];
//
//    self.comments = @[
//        [[Comment alloc] initWithCommentId:@"123123" commentContent:@"我最喜欢听权利巅峰这本书以经听了两遍了，我给主播和作者点赞了，主播讲的生动，作者写作写的完美，支持你们" createTime:1656299271 userInfo:user1 diggCount: 155],
//        [[Comment alloc] initWithCommentId:@"12312223" commentContent:@"《权利巅峰》较之刘飞差的不是一点半点。如果是平民身份倒还说得过去，刘飞儿子的身份，多余无用。" createTime:1656306144 userInfo:user1 diggCount: 70],
//        [[Comment alloc] initWithCommentId:@"12312223" commentContent:@"喜欢梦入洪荒的刘飞，刘小飞，柳擎宇，柳浩天的系列小说，满满的正能量。只是刘小飞的那篇好像没写完，怎么也不更新啊！" createTime:1656306144 userInfo:user1 diggCount: 45],
//    ];
//    self.commentCnt = 127;
    
//}
@end
