//
//  Test.h
//  work
//
//  Created by ByteDance on 2022/7/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Test : NSObject
@property (nonatomic, assign) BOOL isSHow;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, assign) NSInteger num;
@end

NS_ASSUME_NONNULL_END
