//
//  AppDelegate.m
//  ios-app-MVC
//
//  Created by ByteDance on 2022/6/28.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    /*
    // UINavigationController 的使用
    UIViewController* vc = [[UIViewController alloc]init];
    vc.view.backgroundColor = [UIColor redColor];

    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"Root"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:nil];
    vc.navigationItem.rightBarButtonItem = rightItem;
    vc.navigationItem.title = @"Hello, im the title";
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window setRootViewController:naviVC];
    [self.window makeKeyAndVisible];
    */
    
    /*
    //UITabBarController的使用
    UITabBarController *tabBarViewController = [[UITabBarController alloc]init];
    UIViewController* first = [[UIViewController alloc]init];
    first.tabBarItem.title = @"tab 1";
    first.view.backgroundColor = [UIColor redColor];
    UIViewController* second = [[UIViewController alloc]init];
    second.tabBarItem.title = @"tab 2";
    second.view.backgroundColor = [UIColor blueColor];
    tabBarViewController.viewControllers = [NSArray arrayWithObjects:first, second, nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window setRootViewController:tabBarViewController];
    [self.window makeKeyAndVisible];
    */
    
    
    self.window=[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];//创建一个Window
          self.window.backgroundColor=[UIColor blueColor]; //
          self.viewController= [[ViewController alloc] init]; //创建一个VC
          UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:self.viewController];
      //    nav.navigationBar.hidden = YES;
          self.window.rootViewController=nav; //设置好根视图控制器
          [self.window makeKeyAndVisible]; //设置这个window为主(key)窗口并设置成为可见
     
    
    
    return YES;
}




@end
