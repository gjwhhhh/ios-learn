//
//  AppDelegate.h
//  ios-app-MVC
//
//  Created by ByteDance on 2022/6/28.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    UIWindow * window;
    ViewController * viewController;
}
@property(nonatomic) NSMutableArray *tasks;
@property(nonatomic) UITableView *taskTable;
@property(nonatomic) UITextField *taskField;
@property(nonatomic) UIButton *insertButton;

@property (nonatomic, strong) UIWindow * window;
@property (nonatomic, strong) ViewController * viewController;

- (void)addTasks:(id)sender;
@end

