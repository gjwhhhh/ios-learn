//
//  SceneDelegate.h
//  ios-app-MVC
//
//  Created by ByteDance on 2022/6/28.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

